﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>XMLHTTP Enhanced Asynchronous Call Example</title>
	<script type="text/javascript" src="AJAXGeneral.js"></script>

	<script type="text/javascript">

		////step #1
		//function UseResultsCallBack() {
		//	if (this.readyState === READYSTATE_COMPLETE) {
		//		const response = this.response;
		//		const target = document.getElementById("Target");
		//		if (this.status === HTTPSTATUS_OK) {
		//			target.innerHTML = response;
		//		}
		//		else {
		//			target.innerHTML = this.statusText;
		//			return this.statusText;
		//		}
		//	}
		//}

		//var xmlHttpObj;         //in step#1 needs to be defined as a global variable
		//function MakeXMLHTTPCall(file) {
		//	xmlHttpObj = new XMLHttpRequest();
		//	xmlHttpObj.open("GET", "http://" + location.host + "/" + file, true);
		//	xmlHttpObj.onreadystatechange = UseResultsCallBack;
		//	xmlHttpObj.send(null);
		//	return xmlHttpObj;
		//}

		// Step 2
		function UseResultsCallBack(xmlHttpObj, file) {
			if (xmlHttpObj.readyState === READYSTATE_COMPLETE) {
				const response = xmlHttpObj.response;
				const target = document.getElementById("Target");
				if (xmlHttpObj.status === HTTPSTATUS_OK) {
					target.innerHTML = response;
				}
				else {
					target.innerHTML = "File " + file + " could not be found!";
					return xmlHttpObj.statusText;
				}
			}
		}

		function MakeXMLHTTPCall(file) {
			var xmlHttpObj = new XMLHttpRequest();
			xmlHttpObj.open("GET", "http://" + location.host + "/" + file, true);
			xmlHttpObj.onreadystatechange = function() {UseResultsCallBack(xmlHttpObj, file);}
			xmlHttpObj.send(null);
			return xmlHttpObj;
		}

		function OpenPopUp(control) {
			var popup = document.getElementById("PopUp");
			popup.style.left = window.event.clientX + "px";
			popup.style.top = window.event.clientY + "px";
			popup.style.display = "";
			control.style.backgroundColor = "aqua";
			MakeXMLHTTPCall(control.innerText);
			//fill in code here to call MakeXMLHTTPCall() with proper parameter

		}

		function ClosePopUp(control) {
			var popup = document.getElementById("PopUp");
			//popup.style.visibility="hidden";
			popup.style.display = "none";
			control.style.backgroundColor = "white";
		}
	</script>
</head>
<body>
	<form id="form1" runat="server">
		<asp:Image ID="Image1" runat="server" Height="500px" ImageUrl="~/DSC_0067 (2).JPG" Style="z-index: 100; left: 700px; position: absolute; top: 5px;" Width="714px" />

		<div id="PopUp" style="visibility: visible; display: none; z-index: 1000; position: absolute; border: 1px">
			<table width="400px" border="1" bordercolor="black" cellpadding="1" bgcolor="white">
				<tr>
					<td align="center" id="Target">Insert here actual popup content!!! </td>
				</tr>
			</table>
		</div>

		<table id="FileNames" width="350px" border="1" bordercolor="black" style="position: absolute; left: 50px; top: 50px;">
			<tr>
				<th>File Names
				</th>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>SomeText.txt
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>Table.HTML
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>Image.HTML
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>UWSPLogo.html
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>xml_dataset.XML
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>AjaxGeneral.js
				</td>
			</tr>
			<tr>
				<td onmouseover='OpenPopUp(this);' onmouseout='ClosePopUp(this);'>DoesNotExist
				</td>
			</tr>
		</table>
	</form>
</body>
</html>
