﻿// JScript File
// Utility function to obtain a valid XMLHttpRequest object.
// Addresses differences among various browsers
function CreateXmlHttpRequestObject()
{
    var xmlObj;
    if(window.ActiveXObject)               //Microsoft IE
    {
        try
        {
           xmlObj = new ActiveXObject("Microsoft.XMLHTTP"); //if this one fails, then it must be
        } catch (e)                                         //a different version of XML parser          
        {
           xmlObj = new ActiveXObject("Msxml2.XMLHTTP");
        }
    }
    else                                    //other: Mozilla, Firefox, etc 
        xmlObj = new XMLHttpRequest();
        
    return xmlObj;
}
// Common values for the readyState property of the XMLHttpRequest object
const READYSTATE_UNINITIALIZED = 0;
const READYSTATE_LOADING = 1;
const READYSTATE_LOADED = 2;
const READYSTATE_INTERACTIVE = 3;
const READYSTATE_COMPLETE = 4;

// Common values for HTTP status codes as in the status property of the XMLHttpRequest object
const HTTPSTATUS_OK = 200;
const HTTPSTATUS_NOT_FOUND = 404;
const HTTPSTATUS_SERVER_ERROR = 500;

