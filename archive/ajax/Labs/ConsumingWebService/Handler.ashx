﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Linq;
using System.Text;
using System.Web;
using org.oorsprong.webservices;

public class Handler : IHttpHandler {

	public void ProcessRequest (HttpContext context) {
		if (context.Request.QueryString["Request"] == "Continents"){
			context.Response.Write(GetContinents());
		}
		else if ( context.Request.QueryString["Request"] == "ContriesByContinent" )
		{
			context.Response.Write(GetContriesByContinent(context.Request.QueryString["Continent"]));
		}
		else if ( context.Request.QueryString["Request"] == "CountryInfo" )
		{
			context.Response.Write(GetCountryInfo(context.Request.QueryString["Country"]));
		}
	}

	public bool IsReusable {
		get {
			return false;
		}
	}

	private string GetContinents()
	{
		CountryInfoService service = new CountryInfoService();
		tContinent[] continents = service.ListOfContinentsByName();
		StringBuilder response = new StringBuilder("[ ");

		foreach ( tContinent continent in continents )
		{
			response.Append("{\"code\":\"" + continent.sCode + "\", \"name\":\"" + continent.sName + "\"}, ");
		}

		response.Remove(response.Length - 2, 2);
		response.Append(" ]");
		return response.ToString();
	}

	private string GetContriesByContinent(string continent)
	{
		CountryInfoService service = new CountryInfoService();
		tCountryCodeAndNameGroupedByContinent[] countriesAndContinents = service.ListOfCountryNamesGroupedByContinent();
		var countries = countriesAndContinents.Single(item => item.Continent.sCode == continent);

		StringBuilder response = new StringBuilder("[ ");
		foreach ( tCountryCodeAndName country in countries.CountryCodeAndNames )
		{
			response.Append("{\"code\":\"" + country.sISOCode + "\", \"name\":\"" + country.sName + "\"}, ");
		}
		response.Remove(response.Length - 2, 2);
		response.Append(" ]");
		return response.ToString();
	}

	private string GetCountryInfo(string countryCode)
	{
		CountryInfoService service = new CountryInfoService();
		tCountryInfo info = service.FullCountryInfo(countryCode);
		StringBuilder response = new StringBuilder("{ ");
		if ( info.Languages.Any() )
		{
			response.Append("\"capital\":\"" + info.sCapitalCity + "\", \"currency\":\"" + info.sCurrencyISOCode + "\", \"languageName\":\"" + info.Languages[0].sName + "\", \"languageCode\":\"" + info.Languages[0].sISOCode + "\", \"phone\":\"" + info.sPhoneCode + "\", " + GetCountryFlag(countryCode) +" }");
		}
		else
		{
			response.Append("\"capital\":\"" + info.sCapitalCity + "\", \"currency\":\"" + info.sCurrencyISOCode + "\", \"languageName\":\"" + "No Language" + "\", \"languageCode\":\"" + "No Language" + "\", \"phone\":\"" + info.sPhoneCode + "\", " + GetCountryFlag(countryCode) +" }");
		}
		return response.ToString();
	}

	private string GetCountryFlag(string countryCode)
	{
		CountryInfoService service = new CountryInfoService();
		string flagLink = service.CountryFlag(countryCode);
		return "\"flagLink\":\"" + flagLink + "\"";
	}

}