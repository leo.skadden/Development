﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Consuming Web Sevices</title>
	<script src="Resources/modernizr-2.5.2.js"></script>
	<%--<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCekXPvldBz3qW5-jee81w_YGNelXKrnOQ&sensor=false" type="text/javascript"></script>
		<script src="https://maps.googleapis.com/maps/api/js?key= COPY API_KEY HERE &sensor=false" type="text/javascript"></script>
	--%>
	<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB1rfnqQYfdqFfe8BT_T-AGy5tmtNJ3kfE&sensor=false" type="text/javascript"></script>
	<script src="Resources/jquery-1.6.1.min.js"></script>
	<script type="text/javascript" src="AJAXGeneral.js"></script>
	<script type="text/javascript">
		var jQ = jQuery.noConflict();

		var latitude;
		var longitude;
		var geocoder;
		var map;
		var markersArray = [];
		var zoom = 18;

		//execution starts here
		function useGeolocation(position) {
			latitude = position.coords.latitude;
			longitude = position.coords.longitude;

			jQ("#latitude").val(latitude);
			jQ("#longitude").val(longitude);
			startMap();
		}

		function onGeolocationError(error) {
			alert(`Geolocation error - code: ${error.code} message : ${error.message}`);
		}

		jQ(document).ready(function() {

			if (navigator.geolocation) {
				jQ("#geolocation").html("is supported.");
				navigator.geolocation.getCurrentPosition(useGeolocation, onGeolocationError);
			} else if (Modernizr.geolocation) {
				jQ("#geolocation").html("is supported.");
				navigator.geolocation.getCurrentPosition(useGeolocation, onGeolocationError);

			} else {
				jQ("#geolocation").html("is not supported.");
			}
		});


		function placeMarker(whereTo) {
			const markerOptions = {
				position: new google.maps.LatLng(whereTo.lat(), whereTo.lng()),
				map: map
			};
			deleteOverlays();
			const marker = new google.maps.Marker(markerOptions);
			latitude = whereTo.lat();
			longitude = whereTo.lng();
			jQ("#latitude").val(latitude);
			jQ("#longitude").val(longitude);
			markersArray.push(marker);
			findAddress(latitude, longitude);
		}

		function deleteOverlays() {
			if (markersArray) {
				for (i in markersArray) {
					markersArray[i].setMap(null);
				}
				markersArray.length = 0;
			}
		}

		function changeMap() {
			latitude = jQ("#latitude").val();
			longitude = jQ("#longitude").val();
			if (map != null) zoom = map.zoom;
			const myOptions = {
				zoom: zoom,
				center: new google.maps.LatLng(latitude, longitude),
				mapTypeId: google.maps.MapTypeId.HYBRID
			};
			map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
			placeMarker(myOptions.center);
			google.maps.event.addListener(map, 'click', function(event) { placeMarker(event.latLng); });
		}

		function startMap() {
			changeMap();
			PopulateContinentList();
		}
	</script>

	<script type="text/javascript">
		//centers map based on latitude and longitude
		function findAddress(latitude, longitude) {
			const latlng = new google.maps.LatLng(latitude, longitude);
			geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'latLng': latlng },
				function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						if (results[1]) {
							jQ('#Address').html(results[1].formatted_address);
						}
					} else {
						alert(`Geocoder failed due to: ${status}`);
					}
				});
		}

		//centers map based on address
		function setMapAddress(address) { // "London, UK" for example 
			geocoder = new google.maps.Geocoder();
			geocoder.geocode({ 'address': address },
				function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						map.setCenter(results[0].geometry.location);
						//map.fitBounds(results[0].geometry.viewport);
						const marker = new google.maps.Marker({
							map: map,
							position: results[0].geometry.location
						});
					} else
						alert(`Geocode was not successful for the following reason: ${status}`);
				});
		}

		function PopulateContinentList() {
			const xmlHttpObj = CreateXmlHttpRequestObject();
			if (xmlHttpObj) {
				xmlHttpObj.open("GET", `Handler.ashx?Request=Continents`, false);
				xmlHttpObj.send(null);
				const response = JSON.parse(xmlHttpObj.responseText);
				const dropdown = document.getElementById('Continents');
				dropdown.innerHTML = "";
				for (let i = 0; i < response.length; i++) {
					const option = document.createElement("option");
					option.value = response[i]["code"];
					option.text = response[i]["name"];
					dropdown.add(option);
				}
			}
		}

		function NewContinentSelection() {
			const xmlHttpObj = CreateXmlHttpRequestObject();
			const continetDropdown = document.getElementById('Continents');
			if (xmlHttpObj) {
				let continent = continetDropdown.value;
				xmlHttpObj.open("GET", `Handler.ashx?Request=ContriesByContinent&Continent=${continent}`, false);
				xmlHttpObj.send(null);
				const response = JSON.parse(xmlHttpObj.responseText);
				const dropdown = document.getElementById('Countries');
				dropdown.innerHTML = "";
				for (let i = 0; i < response.length; i++) {
					let option = document.createElement("option");
					option.value = response[i]["code"];
					option.text = response[i]["name"];
					dropdown.add(option);
				}
			}
		}

		function NewCountrySelection() {
			const xmlHttpObj = CreateXmlHttpRequestObject();
			const continetDropdown = document.getElementById('Continents');
			const countryDropdown = document.getElementById("Countries");
			const name = document.getElementById("CountryName");
			const code = document.getElementById("CountryCode");
			const capital = document.getElementById("CountryCapital");
			const continent = document.getElementById("CountryContinent");
			const currency = document.getElementById("CountryCurrency");
			const languageName = document.getElementById("CountryLanguageName");
			const languageCode = document.getElementById("CountryLanguageCode");
			const phone = document.getElementById("CountryPhone");
			const flag = document.getElementById("Flag");
			if (xmlHttpObj) {
				let countryCode = countryDropdown.value;
				let countryName = countryDropdown.selectedOptions[0].innerText;
				xmlHttpObj.open("GET", `Handler.ashx?Request=CountryInfo&Country=${countryCode}`, false);
				xmlHttpObj.send(null);
				const response = JSON.parse(xmlHttpObj.responseText);
				name.innerText = countryName;
				code.innerText = countryCode;
				capital.innerText = response["capital"];
				continent.innerText = continetDropdown.value;
				currency.innerText = response["currency"];
				languageName.innerText = response["languageName"];
				languageCode.innerText = response["languageCode"];
				phone.innerText = response["phone"];
				flag.src = response["flagLink"];
				setMapAddress(response["capital"] + ", " + countryCode);
			}

		}
	</script>

</head>
<body>
<form action="javascript:void(0);">
	<table style="float: right; top: 0%;">
		<tr>
			<th>Continents: </th>
			<td>
				<select id="Continents" name="Continents" onchange="NewContinentSelection()"><option>one</option>
					<option>two</option>
				</select>
			</td>
		</tr>
		<tr>
			<th>Countries: </th>
			<td>
				<select id="Countries" name="Countries" onchange="NewCountrySelection()">
					<option>one</option>
					<option>two</option>
				</select>
			</td>
		</tr>
	</table>
	<table id="Country Data" style="left: 50%; position: relative; text-align: left; top: 0%;">
		<tr>
			<td>
				<b>Country Name: </b>
			</td>
			<td id="CountryName">Place Country Name Here...</td>
			<td rowspan="8">
				<img id="Flag" src="http://www.oorsprong.org/WebSamples.CountryInfo/Images/USA.jpg"/>
			</td>
		</tr>
		<tr>
			<td>
				<b>Country Code: </b>
			</td>
			<td id="CountryCode">Place Country Code Here...</td>
		</tr>
		<tr>
			<td>
				<b>Capital City: </b>
			</td>
			<td id="CountryCapital">Place Capital City Here...</td>
		</tr>
		<tr>
			<td>
				<b>Continent Code: </b>
			</td>
			<td id="CountryContinent">Place Continent Code Here...</td>
		</tr>
		<tr>
			<td>
				<b>Currency Code: </b>
			</td>
			<td id="CountryCurrency">Place Currency Code Here...</td>
		</tr>
		<tr>
			<td>
				<b>Language Code: </b>
			</td>
			<td id="CountryLanguageCode">Place Language Code Here...</td>
		</tr>
		<tr>
			<td>
				<b>Language Name: </b>
			</td>
			<td id="CountryLanguageName">Place Language Name Here...</td>
		</tr>
		<tr>
			<td>
				<b>Phone Code: </b>
			</td>
			<td id="CountryPhone">Place Phone Code Here...</td>
		</tr>
	</table>
	<table style="left: 0%; position: absolute; text-align: left; top: 0%;">
		<tr>
			<td><b>Geolocation</b> : <span id="geolocation"></span></td>
		</tr>
		<tr>
			<td>
				<b>Latitude</b> :
				<input onkeydown="if (event.keyCode == 13) changeMap();" type="text" id="latitude" value="0"/>
			</td>
		</tr>
		<tr>
			<td>
				<b>Longitude</b> :
				<input onkeydown="if (event.keyCode == 13) changeMap();" type="text" id="longitude" value="0"/>
				<button onclick="changeMap()">Go</button>
			</td>
		</tr>
		<tr>
			<td><b>Address</b> : <span id="Address"></span></td>
		</tr>
		<tr>
			<td>
				<button onclick="changeMap()">Center Map</button>
			</td>
		</tr>
		<tr>
			<td>
				<button onclick="setMapAddress(document.getElementById('NewAddress').value);">Go To Address</button>
				<input type="text" id="NewAddress"/>
			</td>
		</tr>
	</table>
</form>

<div id="map_canvas" style="height: 800px; width: 100%;"></div>

</body>
</html>