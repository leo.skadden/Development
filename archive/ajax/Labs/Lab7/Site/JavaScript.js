﻿function MakeXMLHTTPCall(file) {
	let xmlHttpObj = new XMLHttpRequest();
	xmlHttpObj.open("GET", "http://" + location.host + "/" + file, false);

	xmlHttpObj.send(null);
	xmlHttpObj = (xmlHttpObj.status !== 200) ? null : xmlHttpObj;
	return xmlHttpObj;

}

function FillConsumerTable(file) {
	const xmlResponse = MakeXMLHTTPCall(file);
	const table = document.getElementById("tableBody");
	const json = JSON.parse(xmlResponse.response);
	const customers = json["Table"]["Consumer_Data"];

	for (let i = 0; i < customers.length; i++)
	{
		const customer = customers[i];
		const row = table.insertRow(-1);
		const cells = MakeCells(row, 5);

		FillCell(cells[0], customer["CName"]);
		FillCell(cells[1], customer["City"]);
		FillCell(cells[2], customer["PName"]);
		FillCell(cells[3], customer["Price"]);
		FillCell(cells[4], customer["Quantity"]);
	}	
}

var masterXmlResponse;

function FillMasterTable(file) {
	masterXmlResponse = MakeXMLHTTPCall(file);
	const masterTable = document.getElementById("masterTableBody");
	const json = JSON.parse(masterXmlResponse.response);
	const customers = json["Table"]["Consumer_Data"];

	for (let i = 0; i < customers.length; i++) {
		const customer = customers[i];
		const products = JSON.stringify(customer["Products"]);
		sessionStorage.setItem(customer["CName"], products);

		const row = masterTable.insertRow(-1);
		const cells = MakeCells(row, 2);
		FillCell(cells[0], customer["CName"]);
		FillCell(cells[1], customer["City"]);
	}
	TableHoverListeners(document.getElementById("masterTableBody"));
}

function TableHoverListeners(table) {
	const rows = table.rows;
	for (let i = 0; i < rows.length; i++) {
		const row = rows[i];
		row.addEventListener("mouseenter", function() { FillDetailTable(row); });
		row.addEventListener("mouseleave", function() { EmptyDetailTable(row); });
	}
}

function FillDetailTable(masterRow) {
	const detailTable = document.getElementById("detailTableBody");
	
	const products = JSON.parse(sessionStorage.getItem(masterRow.cells[0].textContent));

	for (let i = 0; i < products.length; i++) {
		const product = products[i];
		const row = detailTable.insertRow(-1);
		const cells = MakeCells(row, 3);

		FillCell(cells[0], product["PName"]);
		FillCell(cells[1], product["Quantity"]);
		FillCell(cells[2], product["Price"]);
	}
}

function EmptyDetailTable() {
	const detailTable = document.getElementById("detailTableBody");
	detailTable.innerHTML = "";
} 

function MakeCells(row, numCells) {
	const cells = new Array(numCells);
	for (let i = 0; i < numCells; i++) {
		cells[i] = row.insertCell(-1);
	}

	return cells;
}

function FillCell(cell, data) {
	cell.innerHTML = data;
}