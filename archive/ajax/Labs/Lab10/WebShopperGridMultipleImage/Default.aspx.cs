using System;
using System.CodeDom;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DependentGrids : Page
{
	protected void Page_Load(object sender, EventArgs e)
	{
		Ajax.Utility.RegisterTypeForAjax(typeof(DependentGrids));
	}

	[Ajax.AjaxMethod]
	public string LoadPreferredCustomers()
	{
		SqlConnection sqlcn =
			new SqlConnection("Data Source=aeq;Initial Catalog=WebShopping;User ID=WebShopper;Password=cis341");
		DataTable preferredCustomersTable = new DataTable();
		string sqlCommand = "SELECT TOP 20 Tb_PreferredCustomer.Cust_ID,Firstname,LastName,City,State,ZipCode " +
		                    "FROM Tb_PreferredCustomer,Tb_Purchases " +
		                    "WHERE Tb_PreferredCustomer.Cust_ID=Tb_Purchases.Cust_ID " +
		                    "GROUP BY Tb_PreferredCustomer.Cust_ID,Firstname,LastName,City,State,ZipCode " +
		                    "ORDER BY COUNT(Prod_ID) DESC";
		SqlDataAdapter da = new SqlDataAdapter(sqlCommand, sqlcn);
		da.Fill(preferredCustomersTable);
		GridView gw = new GridView
		{
			ID = "PreferredCustomers",
			BackColor = Color.Ivory,
			BorderWidth = 1
		};
		gw.Font.Bold = true;
		gw.DataSource = preferredCustomersTable;
		gw.DataBind();
		return RenderControlToHtml(gw);
	}

	[Ajax.AjaxMethod]
	public string LoadProducts(int custId)
	{
		SqlConnection sqlcn =
			new SqlConnection("Data Source=aeq;Initial Catalog=WebShopping;User ID=WebShopper;Password=cis341");
		DataTable productsTable = new DataTable();
		string sqlCommand =
			"SELECT Tb_AvailableProduct.Prod_ID,Name,Price FROM Tb_PreferredCustomer,Tb_Purchases,Tb_AvailableProduct " +
			"WHERE Tb_PreferredCustomer.Cust_ID=Tb_Purchases.Cust_ID " +
			"AND Tb_Purchases.Prod_ID=Tb_AvailableProduct.Prod_ID " +
			"AND Tb_PreferredCustomer.Cust_ID=" + custId;
		SqlDataAdapter da = new SqlDataAdapter(sqlCommand, sqlcn);
		da.Fill(productsTable);
		if ( productsTable.Rows.Count == 0 )
		{
			productsTable.Rows.Add(productsTable.NewRow());
		}

		GridView gw = new GridView
		{
			ID = "Products",
			DataSource = productsTable
		};
		gw.DataBind();
		gw.BackColor = Color.Ivory;
		gw.BorderWidth = 1;
		gw.Font.Bold = true;
		return RenderControlToHtml(gw);
	}


	[Ajax.AjaxMethod]
	public string GetProductPicture(int prodId)
	{
		SqlConnection sqlcn = new SqlConnection("Data Source=aeq;Initial Catalog=WebShopping;User ID=WebShopper;Password=cis341");

		SqlCommand sqlCommand = new SqlCommand("SELECT [Picture] FROM [dbo].[Tb_AvailableProduct] WHERE [Prod_ID] = " + prodId, sqlcn);
		sqlcn.Open();
		var nullCheck = sqlCommand.ExecuteScalar();
		if ( nullCheck.GetType() == typeof(System.DBNull) )
		{
			return null;
		}
		byte[] image = (byte[]) nullCheck;
		sqlcn.Close();
		return Convert.ToBase64String(image);
	}

	private string RenderControlToHtml(Control controlToRender)
	{
		StringBuilder sb = new StringBuilder();
		StringWriter sw = new StringWriter(sb);
		HtmlTextWriter htmlw = new HtmlTextWriter(sw);
		controlToRender.RenderControl(htmlw);
		return sb.ToString();
	}
}