﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="DependentGrids" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
	<title>Dependent GridViews with Table Sorting</title>
	<script type="text/javascript" src="jslib_event.js"></script>
	<script type="text/javascript" src="jslib_dom_ready.js"></script>
	<script type="text/javascript" src="tablesort_library.js"></script>

	<script type="text/javascript" src="AJAXGeneral.js"></script>

	<script language="javascript" type="text/javascript">
		
		//function to add class attributes to cells in header and rows
		//for the purpose of compliance with the sorting functions
		function AddClassAttributes(table) {
			try {
				const headers = document.getElementById(table).getElementsByTagName("th");
				const rows = document.getElementById(table).getElementsByTagName("tr");
				let cells = null;
				for (let i = 0; i < headers.length; i++)
					headers[i].className = headers[i].firstChild.data;
				for (let j = 0; j < rows.length; j++) {
					cells = rows[j].getElementsByTagName("td");
					for (let k = 0; k < cells.length; k++)
						cells[k].className = headers[k].className;
				}
			} catch (Error) {
			}
		}

		function GetPreferredCustomers() {
			const response = DependentGrids.LoadPreferredCustomers(GetPreferredCustomers_CallBack);
		}

		function GetProducts(custId) {
			const response = DependentGrids.LoadProducts(custId, GetProducts_CallBack);
		}

		function GetPreferredCustomers_CallBack(response) {
			if (response.error != null)
				alert(response.error.name + " - " + response.error.description);
			else {
				const panel1 = document.getElementById("Panel1");
				const panel2 = document.getElementById("Panel2"); 
				panel1.innerHTML = response.value;
				panel2.innerHTML = "";
				InsertAttribute("PreferredCustomers");
				AddClassAttributes("PreferredCustomers");
				//sort table
				jsLib.dom.ready(function() {
					itemList = new TableSort("PreferredCustomers");
				});
			}
		}

		function GetProducts_CallBack(response) {
			if (response.error != null)
				alert(response.error.name + " - " + response.error.description);
			else {
				const panel1 = document.getElementById("Panel1");
				const panel2 = document.getElementById("Panel2");

				panel2.innerHTML = response.value;
				InsertAttribute("Products");
				AddClassAttributes("Products");
				//sort table
				jsLib.dom.ready(function() {
					itemList = new TableSort("Products");
				});
			}
		}

		function AddPicture(response) {
			const img = document.getElementById("ProductImage");
			img.src = "data:image/png;base64," + response.value;
		}

		function GetImage(id) {
			const response = DependentGrids.GetProductPicture(id, AddPicture);
		}

		function InsertAttribute(id) {
			const rows = document.getElementById(id).rows;
			if (id === "PreferredCustomers") {
				for (let i = 1; i < rows.length; i++) {
					const custId = rows[i].cells[0].innerText;
					rows[i].addEventListener("mouseover", function () {
						GetProducts(custId);
						this.style.backgroundColor = "lightblue";
					});
					rows[i].addEventListener("mouseout", function () { this.style.backgroundColor = "ivory" });
				}
			}
			else if (id === "Products") {
				for (let i = 1; i < rows.length; i++) {
					const prodId = rows[i].cells[0].innerText;
					rows[i].addEventListener("mouseover", function () {
						GetImage(prodId);
						this.style.backgroundColor = "lightblue";
					});
					rows[i].addEventListener("mouseout", function () { this.style.backgroundColor = "ivory" });
				}
			}
		}
	</script>
</head>
<body>
<form id="form1" runat="server">
	<asp:Panel ID="Panel1" runat="server" Height="306px" Style="left: 8px; position: absolute; top: 275px; z-index: 100;" Width="400px">
	</asp:Panel>
	<asp:Panel ID="Panel2" runat="server" Height="395px" Style="left: 450px; position: absolute; top: 274px; z-index: 101;" Width="350px">
	</asp:Panel>
	<input id="LoadPreferredCustomers" style="left: 21px; position: absolute; top: 31px; width: 177px; z-index: 104;" type="button" value="LoadPreferredCustomers" onclick="GetPreferredCustomers();"/>
	<asp:Image ID="Image1" runat="server" Height="186px" ImageUrl="~/D5.JPG" Style="left: 207px; position: absolute; top: 16px; z-index: 102;" Width="240px"/>
	<asp:Image ID="Image2" runat="server" ImageUrl="~/D2.JPG" Style="height: 988px; left: 0px; position: absolute; top: 3px; width: 1127px; z-index: 56;"/>
	<asp:Image ID="ProductImage" runat="server" ImageUrl=""
	           Style="height: 251px; left: 700px; position: absolute; top: 13px; width: 415px; z-index: 100;"/>

</form>
</body>
</html>