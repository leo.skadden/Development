﻿<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Web;

public class Handler : IHttpHandler
{
	public void ProcessRequest(HttpContext context)
	{
		if ( context.Request.QueryString["id"] == null )
		{
			return;
		}

		int id = Convert.ToInt32(context.Request.QueryString["id"]);
		context.Response.ContentType = "image/jpeg";
		byte[] bufferImg = GetImage(id);
		context.Response.OutputStream.Write(bufferImg, 0, bufferImg.Length);
	}


	public bool IsReusable
	{
		get { return false; }
	}

	public byte[] GetImage(int id)
	{
		string conn = ConfigurationManager.ConnectionStrings["WebShoppingConnectionString"].ConnectionString;
		SqlConnection connection = new SqlConnection(conn);
		string sql = "SELECT Picture FROM Tb_AvailableProduct WHERE Prod_ID = @Prod_ID";
		SqlCommand cmd = new SqlCommand(sql, connection);
		cmd.CommandType = CommandType.Text;
		cmd.Parameters.AddWithValue("@Prod_ID", id);
		try
		{
			connection.Open();
			return (byte[]) cmd.ExecuteScalar();
		}
		catch
		{
			return null;
		}
		finally
		{
			connection.Close();
		}
	}
}