﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>XMLHttpRequest</title>
	<script type="text/javascript">
		function MakeXMLHTTPCall(file) {
			// ***** This only works in IE so I won't be working with it.*********
			//var xmlHttpObj = new ActiveXObject("Msxml2.XMLHTTP");
			//xmlHttpObj.open("GET", "http://" + location.host + "/Lab4/" + file, false);
			//xmlHttpObj.send(null);
			//return xmlHttpObj;
			let xmlHttpObj = new XMLHttpRequest();
			xmlHttpObj.open("GET", "http://" + location.host + "/" + file, false);
			xmlHttpObj.send(null);
			xmlHttpObj = (xmlHttpObj.status !== 200) ? null : xmlHttpObj;
			return xmlHttpObj;

		}
	</script>
</head>
<body>
<div id="UWSPLogo">
	<img id="Image1" src="UWSPLogo.JPG" style="height: 150px; width: 1200px;"/>
</div>
<div id="SomeText">
	Bring in text here!
</div>
<pre lang="xml" id="XMLDataSet">
	Bring in XMLDataSet here!
</pre>
<div id="Table">

</div>

<script type="text/javascript">

	alert("AJAX start");
	var destination;
	var responseObject;

	// Some text ajax call
	destination = document.getElementById("SomeText");
	responseObject = MakeXMLHTTPCall("SomeText.txt");
	destination.firstChild.data = responseObject == null ? "404 Error: File not found" : responseObject.responseText;

	// Table ajax call
	destination = document.getElementById("Table");
	responseObject = MakeXMLHTTPCall("Table.HTML");

	destination.innerHTML = responseObject == null ? "404 Error: File not found" : responseObject.responseText;

	// XML dataset ajax call
	destination = document.getElementById("XMLDataSet");
	responseObject = MakeXMLHTTPCall("xml_dataset.XML");

	destination.innerText = responseObject == null ? "404 Error: File not found" : responseObject.responseText;

	let suppliers = responseObject.responseXML.getElementsByTagName("Tb_Supplier");
	const joe = suppliers[0];
	const jack = suppliers[1];
	const table = document.getElementById("Seinfeld Family");

	const joeRow = table.insertRow(-1);
	const joeFName = joeRow.insertCell(0);
	const joeLName = joeRow.insertCell(1);
	const joeAge = joeRow.insertCell(2);
	joeFName.innerHTML = joe.childNodes[3].innerHTML;
	joeLName.innerHTML = joe.childNodes[5].innerHTML;
	joeAge.innerHTML = joe.childNodes[7].innerHTML;

	const jackRow = table.insertRow(-1);
	const jackFName = jackRow.insertCell(0);
	const jackLName = jackRow.insertCell(1);
	const jackAge = jackRow.insertCell(2);
	jackFName.innerHTML = jack.childNodes[3].innerHTML;
	jackLName.innerHTML = jack.childNodes[5].innerHTML;
	jackAge.innerHTML = jack.childNodes[7].innerHTML;
</script>
</body>
</html>