﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
	<title>XMLHTTP Asynchronous Call with Parameter Trought HTTP Handler Example</title>
	<script type="text/javascript" src="AJAXGeneral.js"></script>

	<script type="text/javascript">
		//this script block is executed on page load   
		var xmlHttpObj; //needs to be defined as a global variable
		var selectedCity;

		function GetConsumerCities() {
			xmlHttpObj = CreateXmlHttpRequestObject();
			let table = document.getElementById("tableNames").value;
			if (xmlHttpObj) {
				//last parameter on false indicates a synchronous call
				xmlHttpObj.open("POST", `Handler.ashx?Table=${table}`, false);
				xmlHttpObj.send(null);
				//result transfered in JSON format, easy to convert to an array!!!
				const jsonResult = JSON.parse(xmlHttpObj.responseText);

				const ulNode = document.getElementById("consumerCities");
				ulNode.innerHTML = "";
				let listEl;
				let newElementValue;
				for (let c = 0; c < jsonResult.Cities.length; c++) {
					// Create a new list element and add it to the list
					listEl = document.createElement("option");
					ulNode.appendChild(listEl);
					// new element has no content, it needs to be added too
					newElementValue = document.createTextNode(jsonResult.Cities[c].City);
					// this will be the child of the new list element
					listEl.appendChild(newElementValue);
				}
			}
		}

		//this block is executed on city selection change
		function UseConsumersCallBack() {
			if (xmlHttpObj.readyState === READYSTATE_COMPLETE) //if response completed
				if (xmlHttpObj.status === HTTPSTATUS_OK) //and file found then use results
				{
					const table = document.getElementById("consumerNames");
					table.innerHTML = "";
					//result transfered in JSON format, easy to convert to an array!!!
					const jsonResult = JSON.parse(xmlHttpObj.responseText);
					
					for (let c = 0; c < jsonResult.Consumers.length; c++) {
						let row = table.insertRow(-1);
						let id = row.insertCell(-1);
						let name = row.insertCell(-1);
						let city = row.insertCell(-1);
						id.innerText = jsonResult.Consumers[c]["Con_ID"];
						name.innerText = jsonResult.Consumers[c]["Name"];
						city.innerText = jsonResult.Consumers[c]["City"];
					}
				}
		}

		function GetConsumers() {
			xmlHttpObj = CreateXmlHttpRequestObject();

			if (document.getElementById("tableNames").value === "Consumer")
			{
				const selectList = document.getElementById("consumerCities");
				selectedCity = selectList.options[selectList.selectedIndex].text;

				if (xmlHttpObj) {
					//last parameter on true indicates a synchronous call, HttpRequest parameter not in quotes!!!
					xmlHttpObj.open("POST", `Handler.ashx?consumerCity=${selectedCity}`, true);

					//set the UseResultsCallBack() function as listener
					xmlHttpObj.onreadystatechange = UseConsumersCallBack;
					xmlHttpObj.send(null);
				}
			}
			else if (document.getElementById("tableNames").value === "Supplier") {
				const selectList = document.getElementById("consumerCities");
				selectedCity = selectList.options[selectList.selectedIndex].text;

				if (xmlHttpObj) {
					//last parameter on true indicates a synchronous call, HttpRequest parameter not in quotes!!!
					xmlHttpObj.open("POST", `Handler.ashx?supplierCity=${selectedCity}`, true);

					//set the UseResultsCallBack() function as listener
					xmlHttpObj.onreadystatechange = UseConsumersCallBack;
					xmlHttpObj.send(null);
				}
			}
			
		}
	</script>

	<link href="StyleSheet.css" rel="stylesheet" type="text/css" />
</head>
<body onload="GetConsumerCities();">
	<form id="form1" runat="server">
		<div>
			&nbsp;

		&nbsp;List of consumers in the city of:
		</div>
		<select id="tableNames" name="tableNames" style="height: 79px; left: 555px; position: absolute; top: 13px; width: 215px; z-index: 102;" onchange="GetConsumerCities();">
			<option selected="selected" value="Consumer">Consumer Table</option>
			<option value="Supplier">Supplier Table</option>
		</select>	

		<select id="consumerCities" name="consumerCities" style="height: 79px; left: 355px; position: absolute; top: 13px; width: 195px; z-index: 102;" onchange="GetConsumers();">
			<option selected="selected"></option>
		</select>
		<br />
		<br />


		<table id="consumerNames">

		</table>
		<p>
			<asp:Image ID="Image1" runat="server" Height="489px" ImageUrl="~/DSC_0206 (2).JPG" Style="left: 350px; margin-top: 0px; position: absolute; top: 125px; z-index: 101;" Width="792px" />
		</p>
	</form>
</body>
</html>
