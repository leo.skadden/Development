<%@ WebHandler Language="C#" Class="Handler" %>

using System;
using System.Web;
using System.Data.SqlClient;
using System.IO;
using System.Text;

public class Handler : IHttpHandler
{

	public void ProcessRequest(HttpContext context)
	{

		if (context.Request.QueryString["Table"] == "Consumer")
		{
			StringBuilder consumerCities = new StringBuilder("{\"Cities\":[");
			//read city names from Tb_Consumer table
			using (SqlConnection sqlcn =
				new SqlConnection("Data Source=aeq;Initial Catalog=xstudent;User ID=xstudent;Pwd=w20_r]xP|z"))
			{
				sqlcn.Open();
				SqlCommand sqlcmd = new SqlCommand("SELECT DISTINCT City FROM Tb_Consumer", sqlcn);
				using (SqlDataReader cis219Reader = sqlcmd.ExecuteReader())
				{
					while (cis219Reader.Read())
					{
						consumerCities.Append("{\"City\":\"" + cis219Reader["City"].ToString() + "\"},");
					}
					consumerCities.Remove(consumerCities.Length - 1, 1);
					consumerCities.Append("]}");
					sqlcmd.Parameters.Clear();
				}
			}


			//send HTTP response, with consumer cities
			context.Response.Write(consumerCities);
		}
		else if (context.Request.QueryString["Table"] == "Supplier")
		{
			StringBuilder supplierNames = new StringBuilder("{\"Cities\":[");
			//read city names from Tb_Consumer table
			using (SqlConnection sqlcn =
				new SqlConnection("Data Source=aeq;Initial Catalog=xstudent;User ID=xstudent;Pwd=w20_r]xP|z"))
			{
				sqlcn.Open();
				SqlCommand sqlcmd = new SqlCommand("SELECT DISTINCT City FROM Tb_Supplier", sqlcn);
				using (SqlDataReader cis219Reader = sqlcmd.ExecuteReader())
				{
					while (cis219Reader.Read())
					{
						supplierNames.Append("{\"City\":\"" + cis219Reader["City"].ToString() + "\"},");
					}
					supplierNames.Remove(supplierNames.Length - 1, 1);
					supplierNames.Append("]}");
					sqlcmd.Parameters.Clear();
				}
			}


			//send HTTP response, with consumer cities
			context.Response.Write(supplierNames);
		}
		else if (context.Request.QueryString["consumerCity"] != null)
		{
			string consumerCity = context.Request.QueryString["consumerCity"];

			//read consumers from city given as parameter and pack into a string as an HTML list
			SqlConnection sqlcn = new SqlConnection("Data Source=aeq;Initial Catalog=xstudent;User ID=xstudent;Pwd=w20_r]xP|z");
			sqlcn.Open();
			StringBuilder consumerNames = new StringBuilder("{\"Consumers\":[");
			using (SqlCommand sqlcmd = new SqlCommand("SELECT * FROM Tb_Consumer WHERE City=@city", sqlcn))
			{
				SqlParameter conCitypar = new SqlParameter("@city", consumerCity);
				sqlcmd.Parameters.Add(conCitypar);

				using (SqlDataReader cis219Reader = sqlcmd.ExecuteReader())
				{
					while (cis219Reader.Read())
					{
						consumerNames.Append("{ ");
						consumerNames.Append("\"Con_ID\":\"" + cis219Reader["Con_ID"].ToString() + "\",");
						consumerNames.Append("\"Name\":\"" + cis219Reader["Name"].ToString() + "\",");
						consumerNames.Append("\"City\":\"" + cis219Reader["City"].ToString() + "\"");
						consumerNames.Append("},");
					}
					consumerNames.Remove(consumerNames.Length - 1, 1);
					consumerNames.Append("]}");
				}
				sqlcmd.Parameters.Clear();
			}

			//send HTTP response, with consumers from selected city
			context.Response.Write(consumerNames);
		}
		else if (context.Request.QueryString["supplierCity"] != null)
		{
			string consumerCity = context.Request.QueryString["supplierCity"];

			//read consumers from city given as parameter and pack into a string as an HTML list
			SqlConnection sqlcn = new SqlConnection("Data Source=aeq;Initial Catalog=xstudent;User ID=xstudent;Pwd=w20_r]xP|z");
			sqlcn.Open();
			StringBuilder consumerNames = new StringBuilder("{\"Consumers\":[");
			using (SqlCommand sqlcmd = new SqlCommand("SELECT * FROM Tb_Supplier WHERE City=@city", sqlcn))
			{
				SqlParameter conCitypar = new SqlParameter("@city", consumerCity);
				sqlcmd.Parameters.Add(conCitypar);

				using (SqlDataReader cis219Reader = sqlcmd.ExecuteReader())
				{
					while (cis219Reader.Read())
					{
						consumerNames.Append("{ ");
						consumerNames.Append("\"Con_ID\":\"" + cis219Reader["Supp_ID"].ToString() + "\",");
						consumerNames.Append("\"Name\":\"" + cis219Reader["Name"].ToString() + "\",");
						consumerNames.Append("\"City\":\"" + cis219Reader["City"].ToString() + "\"");
						consumerNames.Append("},");
					}
					consumerNames.Remove(consumerNames.Length - 1, 1);
					consumerNames.Append("]}");
				}
				sqlcmd.Parameters.Clear();
			}

			//send HTTP response, with consumers from selected city
			context.Response.Write(consumerNames);
		}
	}

	public bool IsReusable
	{
		get
		{
			return false;
		}
	}
}