function captureForm() {
    const firstName = document.getElementById('firstName').value;
    const lastName = document.getElementById('lastName').value;
    const birthDate = document.getElementById('birthDate').value;

    const formArray = [firstName, lastName, birthDate];

    return formArray;
}

function addTable() {
    const tableDiv = document.getElementById('tableDiv');
    tableDiv.innerHTML = '<table id="table"></table>';
}

function addTableHeader() {
    const table = document.getElementById('table');
    table.innerHTML = '<tr><th>First Name</th><th>Last name</th><th>Birth Date</th>';
}

function addTableData(formArray) {
    if (Array.isArray(formArray)) {
        const table = document.getElementById('table');

        const row = table.insertRow(-1);

        const firstName = row.insertCell(0);
        const lastName = row.insertCell(1);
        const birthDate = row.insertCell(2);

        [firstName.innerHTML, lastName.innerHTML, birthDate.innerHTML] = formArray;
    }
    else {
        TypeError('Didn\'t recieve an array as parameter');
    }
}

function tableExsists() {
    const tableDiv = document.getElementById('tableDiv');
    return tableDiv.children.length !== 0;
}

function clearForm() {
    const firstName = document.getElementById('firstName');
    const lastName = document.getElementById('lastName');
    const birthDate = document.getElementById('birthDate');

    firstName.value = '';
    lastName.value = '';
    birthDate.value = '';
}

function nullInputs(formData) {
    return formData[0] === '' || formData[1] === '' || formData[2] === '';
}

function errorMessage(msg) {
    const errorSpan = document.getElementById('error');
    errorSpan.innerHTML = msg;
}

function clearErrorMessage() {
    const errorSpan = document.getElementById('error');
    errorSpan.innerHTML = '';
}

function submit() {
    const formData = captureForm();
    clearErrorMessage();
    if (nullInputs(formData)) {
        errorMessage('Please fill all fields');
        return;
    }
    if (!tableExsists()) {
        addTable();
        addTableHeader();
    }
    addTableData(formData);
    clearForm();
    return false;
}

function deleteTopRow() {
    if (!tableExsists()) {
        errorMessage("Table doesn't exsist.");
        return;
    }
    const table = document.getElementById('table');
    if (table.rows.length === 2) {
        table.deleteRow(1);
        table.deleteRow(0);
        const tableDiv = document.getElementById('tableDiv');
        tableDiv.innerHTML = '';
    }
    else {
        table.deleteRow(1);
    }
}
