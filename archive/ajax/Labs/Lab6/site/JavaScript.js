﻿function MakeXMLHTTPCall(file) {
	let xmlHttpObj = new XMLHttpRequest();
	xmlHttpObj.open("GET", "http://" + location.host + "/" + file, false);
	xmlHttpObj.send(null);
	xmlHttpObj = (xmlHttpObj.status !== 200) ? null : xmlHttpObj;
	return xmlHttpObj;

}

function FillConsumerTable(file) {
	const xmlResponse = MakeXMLHTTPCall(file);
	const table = document.getElementById("tableBody");
	const customers = xmlResponse.responseXML.getElementsByTagName("Consumer_Data");
	for (let i = 0; i < customers.length; i++)
	{
		const customer = customers[i];
		const row = table.insertRow(-1);
		const cells = MakeCells(row, 5);

		FillCell(cells[0], customer.children[0].textContent);
		FillCell(cells[1], customer.children[1].textContent);
		FillCell(cells[2], customer.children[2].textContent);
		FillCell(cells[3], customer.children[3].textContent);
		FillCell(cells[4], customer.children[4].textContent);
	}	
}

function FillMasterTable(file) {
	const xmlResponse = MakeXMLHTTPCall(file);
	const masterTable = document.getElementById("masterTableBody");
	const names = xmlResponse.responseXML.getElementsByTagName("CName");
	const cities = xmlResponse.responseXML.getElementsByTagName("City");
	for (let i = 0; i < names.length; i++) {
		const name = names[i];
		const city = cities[i];

		const row = masterTable.insertRow(-1);
		const cells = MakeCells(row, 2);
		FillCell(cells[0], name.textContent);
		FillCell(cells[1], city.textContent);
	}
	FillDetailTable(xmlResponse);
}

function FillDetailTable(xmlResponse) {
	const detailTable = document.getElementById("detailTableBody");
	const products = xmlResponse.responseXML.getElementsByTagName("Products");

	for (let i = 0; i < products.length; i++) {
		const product = products[i];
		const row = detailTable.insertRow(-1);
		const cells = MakeCells(row, 3);

		FillCell(cells[0], product.children[0].textContent);
		FillCell(cells[1], product.children[1].textContent);
		FillCell(cells[2], product.children[2].textContent);
	}
}

function MakeCells(row, numCells) {
	const cells = new Array(numCells);
	for (let i = 0; i < numCells; i++) {
		cells[i] = row.insertCell(-1);
	}

	return cells;
}

function FillCell(cell, data) {
	cell.innerHTML = data;
}