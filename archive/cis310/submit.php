<?php
require_once("classes/DataWriter.php");
$writer = new DataWriter;
$string = date('H:i j/M/Y') . ": ";
foreach($_POST as $key => $value) {
	if (isset($value) && !empty($value)) {
		$string .= $key . ": " . $value . ", ";
	}
	else {
		$string .= $key . ": N/A, ";
	}
}
$string .= "\n";
$writer -> write($string);
header("Location: playlistPage.php");
exit;
?>
