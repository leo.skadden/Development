<header>
			<div class='header-cover'>
				<div class='head-title'>
					<h1>WWSP - 90FM</h1>
					<h2>DJ Hub</h2>
				</div>
				<div class='head-logo'><img alt='90fm' src='img/WWSP_90fm_mic.png' style='width:75px;height:90px;'></div>
				<div class='head-login'>
					<?php
						session_start();
						if (!empty($_SESSION["username"])) {
							echo"<div id='logout'><h3>Welcome " . $_SESSION["username"] . "!</h3><br><h4><a href='myAccount.php'>My Account</a>    <a href='includes/logout.php'>Log Out</a></h4></div>";
						}
						else {
							echo "<h3>Login</h3>
							<form method='post'>
								<input name='uid' placeholder='Username/E-mail' required='' type='text'><input name='pwd' placeholder='Password' required='' type='password'><button name='submit' type='submit'>Login</button>
							</form>";
						}
					 ?>
				</div>
			</div>
			<nav>
				<ul>
					<li>
						<a href='prev.php'>Previously Played</a>
					</li>
					<li>
						<a href='log.php'>Playlist Log</a>
					</li>
					<li>
						<a href='reporting.php'>Free-Form Reporting</a>
					</li>
					<li>
						<a href='edit.php'>Admin Edit Page</a>
					</li>
				</ul>
			</nav>
</header>
