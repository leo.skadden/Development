<?php
require_once('classes/Form.php');
$form = new Form();

$formAction = "authenticate.php";

$form->setDefaultTop()
	->startForm($formAction)
	->createLogin()
	->createSubmit()
	->endForm();
print($form->getPage());
?>
