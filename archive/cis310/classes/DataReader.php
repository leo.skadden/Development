<?php
class DataReader {
	private $_file;

	function __construct($file) {
		$this->_file = $file;
	}

	function read() {
		return file_get_contents($this->_file);
	}

	function readUsers() {
		$users = Array();
		$file = $this->read();
		$userAndPass = explode(",", $file);
		for($i = 0; $i < count($userAndPass); $i++){
			$uAP = explode(":",$userAndPass[$i]);
			$users[$uAP[0]] = $uAP[1];
		}
		return $users;
	}

	function readPlaylist() {
		$playlist = Array();
		$fullList = $this->read();
		$entries = explode("\n", $fullList);
		for($i = 0; $i < count($entries); $i++) {
			$temp = explode(":",$entries[$i]);
			$playlist[$i] = $temp;
		}
		return $playlist;
	}
}
?>
