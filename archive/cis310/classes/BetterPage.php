<?php
require_once "Page.php";
require_once $_SERVER["DOCUMENT_ROOT"] . "/header.html";

class BetterPage extends Page{
	private $_middle;

	function __construct($title = 'Default'){
		parent::__construct($title);
	}

	//************Content generation*****************************************
	function setDefaultTop(){
		parent::setHeadSection("<link rel='stylesheet' type='text/css' href='styles/style*'/>");
		parent::setHeadSection("<script src='scripts.js'></script>");
		parent::setTop();
		$this->startWrapper();
		return $this;
	}

	function setMiddle($content, $newLine = True){
		$this->_middle .= ($newline = True ? $content . "\n" : $content);
		return $this;
	}

	function getMiddle(){
		return $this->_middle;
	}

	function getPage(){
		return parent::getTop() . $this->getMiddle() . parent::getBottom();
	}

	function startWrapper() {
		$this->setMidle("<div class='wrapper'>");
		return $this;
	}

	function endWrapper() {
		$this->setMidle("</div>");
		return $this;
	}

	function startMain() {
		$this->setMiddle("<section class='main-container'>")
			->setMiddle("<div class='title-wrapper'>")
			->setMiddle("<h2>" . $this->_pageTitle . "</h2>")
			->setMiddle("</div>")
			->setMiddle("<div class='main-content'>");
		return $this;
	}

	function endMain() {
		$this->setMiddle("</div>")
			->setMiddle("</section>")
			->setMiddle("</div>")
			->getFooter();
	}

	function getHeader() {
		return file_get_contents("/header.php");
	}

	function getFooter() {
		return file_get_contents("/footer.php");
	}

	//******Helper Functions************************************
	function cleanName($name){
		return trim(ucwords($name));
	}

	function checkUser() {
		if (empty($_SESSION["username"])) {
			header("Location: login.php");
		}
	}

	//Return: True if logged in
	function userLoggedIn() {
		return !empty($_SESSION["username"]);
	}

	function getCurrentSongs($playlist) {
		$currentHour = date("H");
		$currentSongs = array();
		foreach ($playlist as $song) {
			$time = $song[0];
			if ($time == $currentHour) {
				array_push($currentSongs, $song);
			}
		}
		return $currentSongs;
	}
}
?>
