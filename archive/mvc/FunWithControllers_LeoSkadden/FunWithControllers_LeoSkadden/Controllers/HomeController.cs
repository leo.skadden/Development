﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;


namespace FunWithControllers_LeoSkadden.Controllers
{
	public class HomeController : Controller
	{
		private List<Person> Persons = new List<Person>();

		public ContentResult Index(string firstName, string lastName, string birthDate)
		{
			if ( firstName != null && lastName != null && birthDate != null )
			{
				Persons.Add(new Person(firstName, lastName, birthDate));
			}
			
			ContentResult content = new ContentResult();

			if ( Persons.Count > 0 )
			{
				content.Content =
					$"<style>table, th, td {{border: 1px solid black;}}</style><form action='/Home/Index' method='get'> <label for='firstName'>First Name</label> <input type='text' id='firstName' value='{firstName}' name='firstName' /> <label for='lastName'>Last Name</label> <input type='text' id='lastName' value='{lastName}' name='lastName'/> <label for='birthDate'>Birth Date</label> <input type='text' id='birthDate' value='{birthDate}' name='birthDate'>  <input type='submit' value='Submit'/> </form> <br/> <table> <tr><th>First Name</th><th>Last Name</th><th>Birth Date</th></tr>";
				foreach ( Person person in Persons )
				{
					content.Content = content.Content +
					                  $"<tr><td>{person.FirstName}</td><td>{person.LastName}</td><td>{person.BirthDate}</td></tr>";
				}
			}
			else
			{
				content.Content =
					$"<form action='/Home/Index' method='get'> <label for='firstName'>First Name</label> <input type='text' id='firstName' value='{firstName}' name='firstName' /> <label for='lastName'>Last Name</label> <input type='text' id='lastName' value='{lastName}' name='lastName'/> <label for='birthDate'>Birth Date</label> <input type='text' id='birthDate' value='{birthDate}' name='birthDate'>  <input type='submit' value='Submit'/> </form>";
			}

			return content;
		}

		private class Person
		{
			public string FirstName { get; set; }
			public string LastName { get; set; }
			public string BirthDate { get; set; }

			public Person(string firstName, string lastName, string birthDate)
			{
				FirstName = firstName;
				LastName = lastName;
				BirthDate = birthDate;
			}
		}
	}
}