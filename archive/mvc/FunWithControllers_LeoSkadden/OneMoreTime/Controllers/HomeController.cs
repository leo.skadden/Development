﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace OneMoreTime.Controllers
{
	public class HomeController : Controller
	{
		[Route("/Home/Index")]
		[HttpGet]
		public ContentResult Index(string firstName, string lastName, string birthDate)
		{
			ContentResult tim = new ContentResult();
			tim.Content =
				$"<form action='/Home/Index' method='get'> <label for='firstName'>First Name</label> <input type='text' id='firstName' value='{firstName}' name='firstName' /> <label for='lastName'>Last Name</label> <input type='text' id='lastName' value='{lastName}' name='lastName'/> <label for='birthDate'>Birth Date</label> <input type='text' id='birthDate' value='{birthDate}' name='birthDate'>  <input type='submit' value='Submit'/> </form>";


			return tim;
		}
	}
}