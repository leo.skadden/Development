﻿namespace Lab2
{
	partial class Form1
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.boxScore = new System.Windows.Forms.TextBox();
			this.boxTotal = new System.Windows.Forms.TextBox();
			this.boxCount = new System.Windows.Forms.TextBox();
			this.boxAverage = new System.Windows.Forms.TextBox();
			this.label1 = new System.Windows.Forms.Label();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.btnAdd = new System.Windows.Forms.Button();
			this.btnDisplay = new System.Windows.Forms.Button();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnExit = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// boxScore
			// 
			this.boxScore.Location = new System.Drawing.Point(92, 41);
			this.boxScore.Name = "boxScore";
			this.boxScore.Size = new System.Drawing.Size(100, 20);
			this.boxScore.TabIndex = 0;
			// 
			// boxTotal
			// 
			this.boxTotal.Location = new System.Drawing.Point(92, 67);
			this.boxTotal.Name = "boxTotal";
			this.boxTotal.Size = new System.Drawing.Size(100, 20);
			this.boxTotal.TabIndex = 1;
			// 
			// boxCount
			// 
			this.boxCount.Location = new System.Drawing.Point(92, 93);
			this.boxCount.Name = "boxCount";
			this.boxCount.Size = new System.Drawing.Size(100, 20);
			this.boxCount.TabIndex = 2;
			// 
			// boxAverage
			// 
			this.boxAverage.Location = new System.Drawing.Point(92, 119);
			this.boxAverage.Name = "boxAverage";
			this.boxAverage.Size = new System.Drawing.Size(100, 20);
			this.boxAverage.TabIndex = 3;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(24, 44);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(38, 13);
			this.label1.TabIndex = 4;
			this.label1.Text = "Score:";
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(24, 70);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(65, 13);
			this.label2.TabIndex = 5;
			this.label2.Text = "Score Total:";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(24, 96);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(69, 13);
			this.label3.TabIndex = 6;
			this.label3.Text = "Score Count:";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(24, 122);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(50, 13);
			this.label4.TabIndex = 7;
			this.label4.Text = "Average:";
			// 
			// btnAdd
			// 
			this.btnAdd.Location = new System.Drawing.Point(198, 38);
			this.btnAdd.Name = "btnAdd";
			this.btnAdd.Size = new System.Drawing.Size(75, 23);
			this.btnAdd.TabIndex = 8;
			this.btnAdd.Text = "&Add";
			this.btnAdd.UseVisualStyleBackColor = true;
			this.btnAdd.Click += new System.EventHandler(this.btnAdd_Click);
			// 
			// btnDisplay
			// 
			this.btnDisplay.Location = new System.Drawing.Point(38, 185);
			this.btnDisplay.Name = "btnDisplay";
			this.btnDisplay.Size = new System.Drawing.Size(97, 23);
			this.btnDisplay.TabIndex = 9;
			this.btnDisplay.Text = "&Display Scores";
			this.btnDisplay.UseVisualStyleBackColor = true;
			this.btnDisplay.Click += new System.EventHandler(this.btnDisplay_Click);
			// 
			// btnClear
			// 
			this.btnClear.Location = new System.Drawing.Point(173, 185);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(75, 23);
			this.btnClear.TabIndex = 10;
			this.btnClear.Text = "&Clear Scores";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// btnExit
			// 
			this.btnExit.Location = new System.Drawing.Point(173, 226);
			this.btnExit.Name = "btnExit";
			this.btnExit.Size = new System.Drawing.Size(75, 23);
			this.btnExit.TabIndex = 11;
			this.btnExit.Text = "E&xit";
			this.btnExit.UseVisualStyleBackColor = true;
			this.btnExit.Click += new System.EventHandler(this.button4_Click);
			// 
			// Form1
			// 
			this.AcceptButton = this.btnAdd;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnExit;
			this.ClientSize = new System.Drawing.Size(284, 261);
			this.Controls.Add(this.btnExit);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.btnDisplay);
			this.Controls.Add(this.btnAdd);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.label1);
			this.Controls.Add(this.boxAverage);
			this.Controls.Add(this.boxCount);
			this.Controls.Add(this.boxTotal);
			this.Controls.Add(this.boxScore);
			this.Name = "Form1";
			this.Text = "Score Calculator";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.TextBox boxScore;
		private System.Windows.Forms.TextBox boxTotal;
		private System.Windows.Forms.TextBox boxCount;
		private System.Windows.Forms.TextBox boxAverage;
		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Button btnAdd;
		private System.Windows.Forms.Button btnDisplay;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnExit;
	}
}

