﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab2
{
	public partial class Form1 : Form
	{
		private List<int> Scores = new List<int>();

		public Form1()
		{
			InitializeComponent();
		}

		private void button4_Click(object sender, EventArgs e)
		{
			Close();
		}

		private void btnAdd_Click(object sender, EventArgs e)
		{
			try
			{
				int score = boxScore.Text == String.Empty ? 0 : Convert.ToInt32(boxScore.Text);
				Scores.Add(score);
				int localCount = Scores.Count;
				UpdateTotal();
				UpdateCount();
				UpdateAverage();
			}
			catch (FormatException exception)
			{
				MessageBox.Show(exception.Message);
			}
			finally
			{
				boxScore.Text = "";
			}
		}

		private void UpdateTotal()
		{
			boxTotal.Text = Scores.Sum().ToString();
		}

		private void UpdateCount()
		{
			boxCount.Text = Scores.Count.ToString();
		}

		private void UpdateAverage()
		{
			boxAverage.Text = Scores.Average().ToString();
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			Scores = new List<int>();
			boxAverage.Text = "";
			boxCount.Text = "";
			boxScore.Text = "";
			boxTotal.Text = "";
		}

		private void btnDisplay_Click(object sender, EventArgs e)
		{
			Scores.Sort();
			string display = "";
			foreach(int score in Scores)
			{
				display += score.ToString() + "\n";
			}
			MessageBox.Show(display);
		}
	}
}
