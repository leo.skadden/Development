﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EntityFrameworkAgain.Models;

namespace EntityFrameworkAgain.Controllers
{
    public class Tb_SupplierController : Controller
    {
        private CIS444Entities db = new CIS444Entities();

        // GET: Tb_Supplier
        public ActionResult Index()
        {
            return View(db.Tb_Supplier.ToList());
        }

        // GET: Tb_Supplier/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Supplier tb_Supplier = db.Tb_Supplier.Find(id);
            if (tb_Supplier == null)
            {
                return HttpNotFound();
            }
            return View(tb_Supplier);
        }

        // GET: Tb_Supplier/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tb_Supplier/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Supp_ID,Name,City")] Tb_Supplier tb_Supplier)
        {
            if (ModelState.IsValid)
            {
                db.Tb_Supplier.Add(tb_Supplier);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tb_Supplier);
        }

        // GET: Tb_Supplier/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Supplier tb_Supplier = db.Tb_Supplier.Find(id);
            if (tb_Supplier == null)
            {
                return HttpNotFound();
            }
            return View(tb_Supplier);
        }

        // POST: Tb_Supplier/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Supp_ID,Name,City")] Tb_Supplier tb_Supplier)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tb_Supplier).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tb_Supplier);
        }

        // GET: Tb_Supplier/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Supplier tb_Supplier = db.Tb_Supplier.Find(id);
            if (tb_Supplier == null)
            {
                return HttpNotFound();
            }
            return View(tb_Supplier);
        }

        // POST: Tb_Supplier/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tb_Supplier tb_Supplier = db.Tb_Supplier.Find(id);
            db.Tb_Supplier.Remove(tb_Supplier);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
