﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using EntityFrameworkAgain.Models;

namespace EntityFrameworkAgain.Controllers
{
    public class Tb_TransactionsController : Controller
    {
        private CIS444Entities db = new CIS444Entities();

        // GET: Tb_Transactions
        public ActionResult Index()
        {
            return View(db.Tb_Transactions.ToList());
        }

        // GET: Tb_Transactions/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Transactions tb_Transactions = db.Tb_Transactions.Find(id);
            if (tb_Transactions == null)
            {
                return HttpNotFound();
            }
            return View(tb_Transactions);
        }

        // GET: Tb_Transactions/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Tb_Transactions/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "Tran_ID,Supp_ID,Con_ID,Prod_ID,Price,Quantity")] Tb_Transactions tb_Transactions)
        {
            if (ModelState.IsValid)
            {
                db.Tb_Transactions.Add(tb_Transactions);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(tb_Transactions);
        }

        // GET: Tb_Transactions/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Transactions tb_Transactions = db.Tb_Transactions.Find(id);
            if (tb_Transactions == null)
            {
                return HttpNotFound();
            }
            return View(tb_Transactions);
        }

        // POST: Tb_Transactions/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "Tran_ID,Supp_ID,Con_ID,Prod_ID,Price,Quantity")] Tb_Transactions tb_Transactions)
        {
            if (ModelState.IsValid)
            {
                db.Entry(tb_Transactions).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(tb_Transactions);
        }

        // GET: Tb_Transactions/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Tb_Transactions tb_Transactions = db.Tb_Transactions.Find(id);
            if (tb_Transactions == null)
            {
                return HttpNotFound();
            }
            return View(tb_Transactions);
        }

        // POST: Tb_Transactions/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Tb_Transactions tb_Transactions = db.Tb_Transactions.Find(id);
            db.Tb_Transactions.Remove(tb_Transactions);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
