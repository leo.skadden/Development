﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
	class Employee
	{
		private const decimal FiftyThousand = 50000.00m;
		private const decimal SeventyThousand = 70000.00m;
		private const decimal OneHundredTwentyThousand = 120000.00m;

		public string Name { get; set; }

		public decimal PackageAmount { get; set; }

		public decimal AfterDeductionPackage { get; set; }

		private Bracket PayBracket { get; set; }

		public decimal PayRate { get; set; }

		public decimal AfterDeductionPayRate { get; set; }

		public decimal Deductions { get; set; }

		public decimal FedTaxes { get; set; }

		public decimal StateTaxes { get; set; }

		public decimal NetPay { get; set; }

		public Employee(string name, decimal pacakgeAmount)
		{
			Name = name;
			PackageAmount = pacakgeAmount;
			
			CalculatePayRate();
			CalculateDeduction();
			CalculateAfterDeductionPayRate();
			CalculateAfterDeductionPackage();
			CalculateAfterDeductionPayBracket();
			
			CalculateFedTaxes();
			CalculateStateTaxes();
			CalculateNetPay();
		}

		private enum Bracket
		{
			First,
			Second,
			Third,
		}

		/// <summary>
		/// Helper method to categorize pay rates after deductions
		/// </summary>
		private void CalculateAfterDeductionPayBracket()
		{
			if (AfterDeductionPackage < FiftyThousand)
			{
				PayBracket = Bracket.First;
			}
			else if (AfterDeductionPackage <= SeventyThousand)
			{
				PayBracket = Bracket.Second;
			}
			else if (AfterDeductionPackage <= OneHundredTwentyThousand)
			{
				PayBracket = Bracket.Third;
			}
			else
			{
				throw new ArgumentOutOfRangeException(nameof(AfterDeductionPackage), $"The employee package amount ({AfterDeductionPackage}) isn't covered.");
			}
		}

		/// <summary>
		/// Calculates the pay rate before taxes or deductions
		/// </summary>
		private void CalculatePayRate()
		{
			PayRate = PackageAmount / 12;
		}

		/// <summary>
		/// Calculates the weekly pay after deductions
		/// </summary>
		private void CalculateAfterDeductionPayRate()
		{
			AfterDeductionPayRate = PayRate - Deductions;
		}

		/// <summary>
		/// Calulates the PF Deduction from before tax package amount.
		/// </summary>
		private void CalculateDeduction()
		{
			const decimal fourPercent = .04m;
			const decimal fivePercent = .05m;
			const decimal sixPercent = .06m;

			if (PackageAmount < FiftyThousand)
			{
				Deductions = (PayRate * fourPercent);
			}
			else if (PackageAmount <= SeventyThousand)
			{
				Deductions = (PayRate * fivePercent);
			}
			else if (PackageAmount <= OneHundredTwentyThousand)
			{
				Deductions = (PayRate * sixPercent);
			}
		}

		/// <summary>
		/// Calulates the package amount after pre-tax deductions
		/// </summary>
		private void CalculateAfterDeductionPackage()
		{
			AfterDeductionPackage = AfterDeductionPayRate * 12;
		}

		/// <summary>
		/// Calculate the federal taxes the employee owes after deductions
		/// </summary>
		private void CalculateFedTaxes()
		{
			const decimal fourteenPercent = .14m;
			const decimal seventeenPercent = .17m;
			const decimal twentyPercent = .20m;

			switch (PayBracket)
			{
				case Bracket.First:
					FedTaxes = AfterDeductionPayRate * fourteenPercent ;
					break;
				case Bracket.Second:
					FedTaxes = AfterDeductionPayRate * seventeenPercent ;
					break;
				case Bracket.Third:
					FedTaxes = AfterDeductionPayRate * twentyPercent ;
					break;
			}
		}

		/// <summary>
		/// Calculates the state taxes after deductions
		/// </summary>
		private void CalculateStateTaxes()
		{
			const decimal sixPercent = .06m;
			const decimal eightPercent = .08m;
			const decimal tenPercent = .10m;

			switch (PayBracket)
			{
				case Bracket.First:
					StateTaxes = AfterDeductionPayRate * sixPercent;
					break;
				case Bracket.Second:
					StateTaxes = AfterDeductionPayRate * eightPercent;
					break;
				case Bracket.Third:
					StateTaxes = AfterDeductionPayRate * tenPercent;
					break;
			}
		}

		/// <summary>
		/// Calculates the net pay after taxes are paid 
		/// </summary>
		private void CalculateNetPay()
		{
			NetPay = AfterDeductionPayRate - StateTaxes - FedTaxes;
		}
	}
}