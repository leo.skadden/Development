﻿using System;
using System.Windows.Forms;

namespace Lab1
{
	public partial class FrmSalaryStatement : Form
	{
		public FrmSalaryStatement()
		{
			InitializeComponent();
		}

		private void btnShow_Click(object sender, EventArgs e)
		{
			try
			{
				string name = boxName.Text;
				decimal package = Convert.ToDecimal(boxPackage.Text);
				Validation.ValidatePackage(package);
				Employee emp = new Employee(name, package);

				boxRate.Text = emp.PayRate.ToString("C");
				boxDeductions.Text = emp.Deductions.ToString("C");
				boxFedTaxes.Text = emp.FedTaxes.ToString("C");
				boxStateTaxes.Text = emp.StateTaxes.ToString("C");
				boxNet.Text = emp.NetPay.ToString("C");
			}
			catch (FormatException exception)
			{
				MessageBox.Show(exception.Message);
			}
			catch (ArgumentOutOfRangeException exception)
			{
				MessageBox.Show(exception.Message);
			}
		}

		private void btnClear_Click(object sender, EventArgs e)
		{
			boxRate.Text = "";
			boxDeductions.Text = "";
			boxFedTaxes.Text = "";
			boxName.Text = "";
			boxNet.Text = "";
			boxPackage.Text = "";
			boxStateTaxes.Text = "";

		}
	}
}
