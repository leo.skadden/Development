﻿namespace Lab1
{
	partial class FrmSalaryStatement
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.label1 = new System.Windows.Forms.Label();
			this.boxName = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.label3 = new System.Windows.Forms.Label();
			this.label4 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.label7 = new System.Windows.Forms.Label();
			this.boxPackage = new System.Windows.Forms.TextBox();
			this.boxRate = new System.Windows.Forms.TextBox();
			this.boxDeductions = new System.Windows.Forms.TextBox();
			this.boxFedTaxes = new System.Windows.Forms.TextBox();
			this.boxStateTaxes = new System.Windows.Forms.TextBox();
			this.boxNet = new System.Windows.Forms.TextBox();
			this.btnClear = new System.Windows.Forms.Button();
			this.btnShow = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(12, 16);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(84, 13);
			this.label1.TabIndex = 0;
			this.label1.Text = "Employee Name";
			// 
			// boxName
			// 
			this.boxName.Location = new System.Drawing.Point(172, 12);
			this.boxName.Name = "boxName";
			this.boxName.Size = new System.Drawing.Size(100, 20);
			this.boxName.TabIndex = 1;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(12, 50);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(92, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Total Package ($)";
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(12, 84);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(66, 13);
			this.label3.TabIndex = 3;
			this.label3.Text = "Pay Rate ($)";
			// 
			// label4
			// 
			this.label4.AutoSize = true;
			this.label4.Location = new System.Drawing.Point(12, 118);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(92, 13);
			this.label4.TabIndex = 4;
			this.label4.Text = "PF Deductions ($)";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(12, 152);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(89, 13);
			this.label5.TabIndex = 5;
			this.label5.Text = "Federal Taxes ($)";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(12, 186);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(79, 13);
			this.label6.TabIndex = 6;
			this.label6.Text = "State Taxes ($)";
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(12, 220);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(60, 13);
			this.label7.TabIndex = 7;
			this.label7.Text = "Net Pay ($)";
			// 
			// boxPackage
			// 
			this.boxPackage.Location = new System.Drawing.Point(172, 46);
			this.boxPackage.Name = "boxPackage";
			this.boxPackage.Size = new System.Drawing.Size(100, 20);
			this.boxPackage.TabIndex = 2;
			// 
			// boxRate
			// 
			this.boxRate.Location = new System.Drawing.Point(172, 80);
			this.boxRate.Name = "boxRate";
			this.boxRate.ReadOnly = true;
			this.boxRate.Size = new System.Drawing.Size(100, 20);
			this.boxRate.TabIndex = 9;
			// 
			// boxDeductions
			// 
			this.boxDeductions.Location = new System.Drawing.Point(172, 114);
			this.boxDeductions.Name = "boxDeductions";
			this.boxDeductions.ReadOnly = true;
			this.boxDeductions.Size = new System.Drawing.Size(100, 20);
			this.boxDeductions.TabIndex = 10;
			// 
			// boxFedTaxes
			// 
			this.boxFedTaxes.Location = new System.Drawing.Point(172, 148);
			this.boxFedTaxes.Name = "boxFedTaxes";
			this.boxFedTaxes.ReadOnly = true;
			this.boxFedTaxes.Size = new System.Drawing.Size(100, 20);
			this.boxFedTaxes.TabIndex = 11;
			// 
			// boxStateTaxes
			// 
			this.boxStateTaxes.Location = new System.Drawing.Point(172, 182);
			this.boxStateTaxes.Name = "boxStateTaxes";
			this.boxStateTaxes.ReadOnly = true;
			this.boxStateTaxes.Size = new System.Drawing.Size(100, 20);
			this.boxStateTaxes.TabIndex = 12;
			// 
			// boxNet
			// 
			this.boxNet.Location = new System.Drawing.Point(172, 216);
			this.boxNet.Name = "boxNet";
			this.boxNet.ReadOnly = true;
			this.boxNet.Size = new System.Drawing.Size(100, 20);
			this.boxNet.TabIndex = 13;
			// 
			// btnClear
			// 
			this.btnClear.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.btnClear.Location = new System.Drawing.Point(29, 261);
			this.btnClear.Name = "btnClear";
			this.btnClear.Size = new System.Drawing.Size(75, 23);
			this.btnClear.TabIndex = 3;
			this.btnClear.Text = "Clear";
			this.btnClear.UseVisualStyleBackColor = true;
			this.btnClear.Click += new System.EventHandler(this.btnClear_Click);
			// 
			// btnShow
			// 
			this.btnShow.Location = new System.Drawing.Point(161, 261);
			this.btnShow.Name = "btnShow";
			this.btnShow.Size = new System.Drawing.Size(111, 23);
			this.btnShow.TabIndex = 4;
			this.btnShow.Text = "&Show Statement";
			this.btnShow.UseVisualStyleBackColor = true;
			this.btnShow.Click += new System.EventHandler(this.btnShow_Click);
			// 
			// FrmSalaryStatement
			// 
			this.AcceptButton = this.btnShow;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.btnClear;
			this.ClientSize = new System.Drawing.Size(284, 296);
			this.Controls.Add(this.btnShow);
			this.Controls.Add(this.btnClear);
			this.Controls.Add(this.boxNet);
			this.Controls.Add(this.boxStateTaxes);
			this.Controls.Add(this.boxFedTaxes);
			this.Controls.Add(this.boxDeductions);
			this.Controls.Add(this.boxRate);
			this.Controls.Add(this.boxPackage);
			this.Controls.Add(this.label7);
			this.Controls.Add(this.label6);
			this.Controls.Add(this.label5);
			this.Controls.Add(this.label4);
			this.Controls.Add(this.label3);
			this.Controls.Add(this.label2);
			this.Controls.Add(this.boxName);
			this.Controls.Add(this.label1);
			this.Name = "FrmSalaryStatement";
			this.Text = "Salary Statement";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label label1;
		private System.Windows.Forms.TextBox boxName;
		private System.Windows.Forms.Label label2;
		private System.Windows.Forms.Label label3;
		private System.Windows.Forms.Label label4;
		private System.Windows.Forms.Label label5;
		private System.Windows.Forms.Label label6;
		private System.Windows.Forms.Label label7;
		private System.Windows.Forms.TextBox boxPackage;
		private System.Windows.Forms.TextBox boxRate;
		private System.Windows.Forms.TextBox boxDeductions;
		private System.Windows.Forms.TextBox boxFedTaxes;
		private System.Windows.Forms.TextBox boxStateTaxes;
		private System.Windows.Forms.TextBox boxNet;
		private System.Windows.Forms.Button btnClear;
		private System.Windows.Forms.Button btnShow;
	}
}

