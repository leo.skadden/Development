﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab1
{
	static class Validation
	{
		/// <summary>
		/// Checks the total package amount for validity
		/// </summary>
		/// <param name="packageAmount">The package total to check.</param>
		/// <returns>A null exception if the pacakge was valid, non-null exception otherwise.</returns>
		internal static void ValidatePackage(decimal packageAmount)
		{
			string errorMsgStart = "Total package cannot be ";
			if (packageAmount <= 0)
			{
				throw new ArgumentOutOfRangeException(nameof(packageAmount), $"{errorMsgStart}less than zero.");
			}
			if (packageAmount > 120000)
			{
				throw new ArgumentOutOfRangeException(nameof(packageAmount), $"{errorMsgStart}more than 120,000.00");
			}
		}
	}
}
