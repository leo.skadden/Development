﻿using System;
using System.Linq;
using System.Web.Mvc;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class CustomerController : Controller
	{
		// GET
		public ActionResult Index()
		{
			ObjectFactory factory = new ObjectFactory();
			return View(factory.Customers);
		}

		[HttpGet]
		public ActionResult CreateCustomer()
		{
			return View("Create", new Customer());
		}

		public ActionResult CreateCustomer(Customer customer)
		{
			if ( !ModelState.IsValid )
			{
				return View("Create", customer);
			}

			ObjectFactory factory = new ObjectFactory();
			factory.SaveCustomer(customer);
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult EditCustomer(int id)
		{
			ObjectFactory factory = new ObjectFactory();
			Customer customer = factory.Customers[id]; 
			
			ViewBag.id = id;
			return View("Edit", customer);
		}

		public ActionResult EditCustomer(Customer customer)
		{
			if (!ModelState.IsValid)
			{
				return View("Edit");
			}
			ObjectFactory factory = new ObjectFactory();
			factory.UpdateCustomer(customer.Id, customer);
			return RedirectToAction("Index");
		}

		public ActionResult DeleteCustomer(int id, bool confirm)
		{
			if ( confirm )
			{
				ObjectFactory factory = new ObjectFactory();
				factory.DeleteCustomer(id);
			}

			return RedirectToAction("Index");
		}
	}
}