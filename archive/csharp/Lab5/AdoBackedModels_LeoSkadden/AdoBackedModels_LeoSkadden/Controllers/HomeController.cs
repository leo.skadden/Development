﻿using System.Web.Mvc;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class HomeController : Controller
	{
		// GET: Home
		public ActionResult Index()
		{
			return View();
		}
	}
}