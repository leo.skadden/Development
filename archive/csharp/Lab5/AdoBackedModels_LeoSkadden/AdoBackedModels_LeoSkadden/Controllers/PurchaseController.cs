﻿using System;
using System.Web.Mvc;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class PurchaseController : Controller
	{
		// GET: Purchase
		public ActionResult Index()
		{
			ObjectFactory factory = new ObjectFactory();
			return View(factory.Purchases);
		}

		[HttpGet]
		public ActionResult CreatePurchase()
		{
			return View("Create", new Purchase());
		}

		public ActionResult CreatePurchase(Purchase purchase)
		{
			if ( !ModelState.IsValid )
			{
				return View("Create", purchase);
			}
			ObjectFactory factory = new ObjectFactory();

			Customer customer = factory.Customers[int.Parse(Request.Form["CustId"])];
			customer.Id = int.Parse(Request.Form["CustId"]);
			Product product = factory.Products[int.Parse(Request.Form["ProdId"])];
			product.Id = int.Parse(Request.Form["ProdId"]);

			purchase.Customer = customer;
			purchase.Product = product;
			factory.SavePurchase(purchase);
			return RedirectToAction("Index");
		}

		[HttpGet]
		public ActionResult EditPurchase(int id)
		{
			ObjectFactory factory = new ObjectFactory();
			Purchase purchase = factory.Purchases[id];

			ViewBag.id = id;
			return View("Edit", purchase);
		}

		public ActionResult EditPurchase(Purchase purchase)
		{
			if ( !ModelState.IsValid )
			{
				return View("Edit", purchase);
			}
			ObjectFactory factory = new ObjectFactory();

			Customer customer = factory.Customers[int.Parse(Request.Form["CustId"])];
			customer.Id = int.Parse(Request.Form["CustId"]);

			Product product = factory.Products[int.Parse(Request.Form["ProdId"])];
			product.Id = int.Parse(Request.Form["ProdId"]);

			purchase = new Purchase(customer, product, DateTime.Parse(Request.Form["PurchaseDate"]), decimal.Parse(Request.Form["Price"]));
			factory.UpdatePurchase(purchase.Id, purchase);
			return RedirectToAction("Index");
		}

		public ActionResult DeletePurchase(int id, bool confirm)
		{
			if ( confirm )
			{
				ObjectFactory factory = new ObjectFactory();
				factory.DeletePurchase(id);
			}

			return RedirectToAction("Index");
		}
	}
}