﻿using System.Linq;
using System.Web.Mvc;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class ListsController : Controller
	{
		// GET: Lists
		public ActionResult Index()
		{
			return View();
		}

		public ActionResult Customer()
		{
			ObjectFactory factory = new ObjectFactory();
			return View("CustomerView", factory.Customers.Values.ToList());
		}

		public ActionResult Product()
		{
			ObjectFactory factory = new ObjectFactory();
			return View("ProductView", factory.Products.Values.ToList());
		}

		public ActionResult Purchase()
		{
			ObjectFactory factory = new ObjectFactory();
			return View("PurchaseView", factory.Purchases);
		}
	}
}