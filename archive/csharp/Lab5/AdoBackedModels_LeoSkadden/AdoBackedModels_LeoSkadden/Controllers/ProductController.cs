﻿using System;
using System.IO;
using System.Web;
using System.Web.Mvc;
using System.Web.Services.Protocols;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class ProductController : Controller
	{
		// GET: Product
		public ActionResult Index()
		{
			ObjectFactory factory = new ObjectFactory();
			return View(factory.Products);
		}

		[HttpGet]
		public ActionResult CreateProduct()
		{
			return View("Create", new Product());
		}

		public ActionResult CreateProduct(Product product)
		{
			HttpPostedFileWrapper pic = (HttpPostedFileWrapper)Request.Files["pic"];
			BinaryReader reader = new BinaryReader(pic.InputStream);
			product.Picture = reader.ReadBytes(pic.ContentLength);

			ObjectFactory factory = new ObjectFactory();
			factory.SaveProduct(product);
			return RedirectToAction("Index");
		}

		public ActionResult EditProduct(int id)
		{
			ObjectFactory factory = new ObjectFactory();
			Product product;
			if ( Request.HttpMethod == "GET" )
			{
				product = factory.Products[id];
				ViewBag.id = id;
				return View("Edit", product);
			}
			else
			{
				byte[] picture = factory.Products[id].Picture;
				if ( Request.Files["Pic"] != null )
				{
					picture = null;
					HttpPostedFileWrapper pic = (HttpPostedFileWrapper) Request.Files["pic"];
					BinaryReader reader = new BinaryReader(pic.InputStream);
					picture = reader.ReadBytes(pic.ContentLength);
				}

				product = new Product(Request.Form["Name"], Convert.ToDecimal(Request.Form["Price"]), Request.Form["Description"], Request.Form["Publisher"], Request.Form["Category"], picture);
				factory.UpdateProduct(id, product);
				return RedirectToAction("Index");
			}
		}

		public ActionResult DeleteProduct(int id, bool confirm)
		{
			if ( confirm )
			{
				ObjectFactory factory = new ObjectFactory();
				factory.DeleteProduct(id);
			}

			return RedirectToAction("Index");
		}
	}
}