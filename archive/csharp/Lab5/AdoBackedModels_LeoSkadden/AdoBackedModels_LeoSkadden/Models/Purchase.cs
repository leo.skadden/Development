﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Purchase
	{
		public readonly string CustomerFirstName;
		public readonly string CustomerLastName;
		public readonly string ProductName;

		public Purchase()
		{
		}

		public Purchase(Customer customer, Product product, DateTime purchaseDate, decimal price)
		{
			PurchaseDate = purchaseDate;
			Customer = customer;
			Product = product;
			Price = price;
			CustomerFirstName = Customer.FirstName;
			CustomerLastName = Customer.LastName;
			ProductName = Product.Name;
		}

		public int Id { get; set; }

		public Customer Customer { get; set; }

		public Product Product { get; set; }

		public int CustId { get; set; }

		public int ProdId { get; set; }

		[Required(ErrorMessage = "Please enter a purchase date.")]
		[DataType(DataType.Date)]
		public DateTime PurchaseDate { get; set; }

		[Required(ErrorMessage = "Please enter a purchase price.")]
		[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid price")]
		public decimal Price { get; set; }
	}
}