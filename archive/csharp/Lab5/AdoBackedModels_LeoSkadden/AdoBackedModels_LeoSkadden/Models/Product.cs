﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Product
	{
		public Product()
		{
		}

		public Product(string name, decimal price, string description, string publisher, string category)
		{
			Name = name;
			Price = price;
			Description = description;
			Publisher = publisher;
			Category = category;
		}

		public Product(string name, decimal price, string description, string publisher, string category, byte[] picture)
		{
			Name = name;
			Price = price;
			Description = description;
			Publisher = publisher;
			Category = category;
			Picture = picture;
		}

		public int Id { get; set; }

		[Required(ErrorMessage = "Please enter a Name.")]
		[RegularExpression(@"[A-Z][a-z]+", ErrorMessage = "Invalid Name.  Name must be capitalized and only letters.")]
		public string Name { get; set; }

		[Required(ErrorMessage = "Please enter a price.")]
		[RegularExpression(@"\d+(\.\d{1,2})?", ErrorMessage = "Invalid price. Must be in the form 0.00")]
		public decimal Price { get; set; }

		public string Description { get; set; }

		public byte[] Picture { get; set; }

		public string Publisher { get; set; }

		public string Category { get; set; }

		public string PictureBase64 => Convert.ToBase64String(Picture ?? new byte[0]);

		public override string ToString()
		{
			return Name;
		}
	}
}