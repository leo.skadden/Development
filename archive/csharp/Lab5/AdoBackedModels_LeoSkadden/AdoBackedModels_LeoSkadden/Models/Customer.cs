﻿using System;
using System.ComponentModel.DataAnnotations;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Customer
	{
		public Customer()
		{
		}

		public Customer(string firstName, string lastName, string address, string city, string state, string zip,
			string phone, string login, string password, DateTime birthDate, string comments, decimal purchases)
		{
			FirstName = firstName;
			LastName = lastName;
			Address = address;
			City = city;
			State = state;
			Zip = zip;
			Phone = phone;
			Login = login;
			Password = password;
			BirthDate = birthDate;
			Comments = comments;
			PurchasesToDate = purchases;
		}

		public int Id { get; set; }

		[Required(ErrorMessage = "Please enter a First Name.")]
		[RegularExpression(@"[A-Z][a-z]+", ErrorMessage = "Invalid First Name. Name must be capitalized and only letters.")]
		public string FirstName { get; set; }

		[Required(ErrorMessage = "Please enter a Last Name.")]
		[RegularExpression(@"[A-Z][a-z]+", ErrorMessage = "Invalid last Name.  Name must be capitalized and only letters.")]
		public string LastName { get; set; }

		// Using regex for address validation is almost impossible. For instance there is a town in California called carmel that doesn't use house numbers, this town would break almost every regex people would come up with.
		public string Address { get; set; }

		[RegularExpression(@"[A-Za-z\s]+", ErrorMessage = "Invalid city.")]
		public string City { get; set; }

		public string State { get; set; }

		[MinLength(5, ErrorMessage = "Invalid zip code.")]
		[MaxLength(5, ErrorMessage = "Invalid zip code.")]
		[RegularExpression(@"\d+", ErrorMessage = "Zip codes can only be numbers. (Sorry Canada)")]
		public string Zip { get; set; }

		[RegularExpression(@"((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}", ErrorMessage = "Invalid phone number.")]
		public string Phone { get; set; }

		[Required(ErrorMessage = "Please enter a Login.")]
		[MinLength(5, ErrorMessage = "Login must be longer than 5 characters.")]
		[RegularExpression(@"[A-Za-z\d]+", ErrorMessage = "Login must be numbers or letters.")]
		public string Login { get; set; }

		[Required(ErrorMessage = "Please enter a Password.")]
		[MinLength(9, ErrorMessage = "Password must be longer than 9 characters.")]
		[RegularExpression(@"[A-Za-z\d]+", ErrorMessage = "Password must be numbers or letters.")]
		public string Password { get; set; }

		[Required(ErrorMessage = "Please enter a Birth Date.")]
		[DataType(DataType.Date)]
		public DateTime BirthDate { get; set; }

		public string Comments { get; set; }

		public decimal PurchasesToDate { get; set; }

		public override string ToString()
		{
			return FirstName + LastName;
		}
	}
}