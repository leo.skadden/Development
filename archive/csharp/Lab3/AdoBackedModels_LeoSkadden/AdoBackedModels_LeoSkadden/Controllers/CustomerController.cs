﻿using System;
using System.Linq;
using System.Web.Mvc;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class CustomerController : Controller
	{
		// GET
		public ActionResult Index()
		{
			ObjectFactory factory = new ObjectFactory();
			return View(factory.Customers);
		}

		[HttpGet]
		public ActionResult CreateCustomer()
		{
			return View("Create", new Customer());
		}

		public ActionResult CreateCustomer(Customer customer)
		{
			ObjectFactory factory = new ObjectFactory();
			factory.SaveCustomer(customer);
			return RedirectToAction("Index");
		}

		public ActionResult EditCustomer(int id)
		{
			ObjectFactory factory = new ObjectFactory();
			Customer customer;
			if ( Request.HttpMethod == "GET" )
			{
				customer = factory.Customers[id];
				ViewBag.id = id;
				return View("Edit", customer);
			}
			customer = new Customer(Request.Form["FirstName"], Request.Form["LastName"], Request.Form["Address"], Request.Form["City"], Request.Form["State"], Request.Form["Zip"], Request.Form["Phone"], Request.Form["Login"], Request.Form["Password"], DateTime.Parse(Request.Form["BirthDate"]), Request.Form["Comments"], Convert.ToDecimal(Request.Form["Purchases"]));
			factory.UpdateCustomer(id, customer);
			return RedirectToAction("Index");
		}

		public ActionResult DeleteCustomer(int id, bool confirm)
		{
			if ( confirm )
			{
				ObjectFactory factory = new ObjectFactory();
				factory.DeleteCustomer(id);
			}

			return RedirectToAction("Index");
		}
	}
}