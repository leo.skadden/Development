﻿using System.Web.Mvc;
using UsingViewsAndModels_LeoSkadden.Models;

namespace UsingViewsAndModels_LeoSkadden.Controllers
{
	public class PurchaseController : Controller
	{
		// GET: Purchase
		public ActionResult Index()
		{
			ObjectFactory factory = new ObjectFactory();
			return View(factory.Purchases);
		}

		[HttpGet]
		public ActionResult CreateProduct()
		{
			return View("Create", new Purchase());
		}

		public ActionResult CreatePurchase(Purchase purchase)
		{
			ObjectFactory factory = new ObjectFactory();
			factory.SavePurchase(purchase);
			return RedirectToAction("Index");
		}

		public ActionResult EditPurchase(int id)
		{
			ObjectFactory factory = new ObjectFactory();
			Purchase purchase;
			if ( Request.HttpMethod == "GET" )
			{
				purchase = factory.Purchases[id];
				ViewBag.id = id;
				return View("Edit", purchase);
			}
			purchase = new Purchase();
			factory.UpdatePurchase(id, purchase);
			return RedirectToAction("Index");
		}

		public ActionResult DeletePurchase(int id, bool confirm)
		{
			if ( confirm )
			{
				ObjectFactory factory = new ObjectFactory();
				factory.DeletePurchase(id);
			}

			return RedirectToAction("Index");
		}
	}
}