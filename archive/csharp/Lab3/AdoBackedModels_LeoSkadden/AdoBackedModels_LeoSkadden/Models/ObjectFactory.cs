﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web.Configuration;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class ObjectFactory
	{
		public Dictionary<int, Customer> Customers = new Dictionary<int, Customer>();
		public Dictionary<int, Product> Products = new Dictionary<int, Product>();
		public Dictionary<int, Purchase> Purchases = new Dictionary<int, Purchase>();
		private const string CustomerTable = "Tb_PreferredCustomer";
		private const string ProductTable = "Tb_AvailableProduct";
		private const string PurchaseTable = "Tb_Purchases";


		public ObjectFactory(string connectionStringName = "cis341Connection")
		{
			string connectionString = ConfigurationManager.ConnectionStrings[connectionStringName].ConnectionString;
			try
			{
				using ( SqlConnection connection = new SqlConnection(connectionString) )
				{
					connection.Open();

					SqlCommand customerCommand = new SqlCommand("SELECT * FROM Tb_PreferredCustomer", connection);
					SqlCommand productCommand = new SqlCommand("SELECT * FROM Tb_AvailableProduct", connection);
					SqlCommand purchaseCommand = new SqlCommand("SELECT * FROM Tb_Purchases", connection);

					using ( SqlDataReader customerReader = customerCommand.ExecuteReader() )
					{
						// This using statement defines the lifetime of the customer reader
						while ( customerReader.Read() )
						{
							// Get all customers from the database and add to collection
							int id = Convert.ToInt32(customerReader["Cust_ID"]);
							string fName = customerReader["FirstName"].ToString();
							string lName = customerReader["LastName"].ToString();
							string address = customerReader["Address"].ToString();
							string city = customerReader["City"].ToString();
							string state = customerReader["State"].ToString();
							string zip = customerReader["ZipCode"].ToString();
							string pNum = customerReader["PhoneNumber"].ToString();
							string login = customerReader["LoginName"].ToString();
							string pass = customerReader["Password"].ToString();
							DateTime birth = Convert.ToDateTime(customerReader["BirthDate"]);
							string comments = customerReader["Comments"].ToString();
							decimal numPurchases = Convert.ToDecimal(customerReader["PurchasesToDate"]);

							Customer cust = new Customer(fName, lName, address, city, state, zip, pNum, login, pass, birth, comments,
								numPurchases);

							Customers.Add(id, cust);
						}
					}

					using ( SqlDataReader productReader = productCommand.ExecuteReader() )
					{
						// This using statement defines the lifetime of the product reader
						while ( productReader.Read() )
						{
							// Get all products from the database and add to collection
							int id = Convert.ToInt32(productReader["Prod_ID"]);
							string name = productReader["Name"].ToString();
							decimal price = Convert.ToDecimal(productReader["Price"]);
							string desc = productReader["Description"].ToString();
							string publisher = productReader["Publisher"].ToString();
							string category = productReader["Category"].ToString();

							Product prod = new Product(name, price, desc, publisher, category);

							Products.Add(id, prod);
						}
					}

					using ( SqlDataReader purchaseReader = purchaseCommand.ExecuteReader() )
					{
						// This using statement defines the lifetime of the purchase reader
						while ( purchaseReader.Read() )
						{
							// Get all purchases from the database and add to collection
							int id = Convert.ToInt32(purchaseReader["Purchase_ID"]);
							int custId = Convert.ToInt32(purchaseReader["Cust_ID"]);
							int prodId = Convert.ToInt32(purchaseReader["Prod_ID"]);
							DateTime purchaseDate = Convert.ToDateTime(purchaseReader["Purchase_Date"]);
							decimal price = Convert.ToDecimal(purchaseReader["Purchase_Price"]);
							Customer cust = Customers[custId];
							Product prod = Products[prodId];

							Purchase purchase = new Purchase(cust, prod, purchaseDate, price);

							Purchases.Add(id, purchase);
						}
					}
				}
			}
			catch ( Exception e )
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void SaveCustomer(Customer customer)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using ( SqlConnection connection = new SqlConnection(connectionString) )
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"INSERT INTO {CustomerTable} (FirstName, LastName, Address, City, State, ZipCode, PhoneNumber, LoginName, Password, BirthDate, Comments, PurchasesToDate) VALUES ('{customer.FirstName}', '{customer.LastName}', '{customer.Address}', '{customer.City}', '{customer.State}', '{customer.Zip}', '{customer.Phone}', '{customer.Login}', '{customer.Password}', '{customer.BirthDate}', '{customer.Comments}', {customer.PurchasesToDate});", connection);
					command.ExecuteNonQuery();
					Customers.Add(Customers.Keys.Last() + 1, customer);
				}
			}
			catch ( SqlException e )
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void DeleteCustomer(int id)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"DELETE FROM {CustomerTable} WHERE [Cust_ID] = {id}", connection);
					command.ExecuteNonQuery();
					Customers.Remove(id);
				}
			}
			catch ( SqlException e )
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void UpdateCustomer(int id, Customer customer)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"UPDATE {CustomerTable} SET ([FirstName] = '{customer.FirstName}', [LastName] = '{customer.LastName}', [Address] = '{customer.Address}', [City] = '{customer.City}', [State] = '{customer.State}', [ZipCode] = '{customer.Zip}', [PhoneNumber] = '{customer.Phone}', [LoginName] = '{customer.Login}', [Password] = '{customer.Password}', [BirthDate] = '{customer.BirthDate}', [Comments] = '{customer.Comments}', [PurchasesToDate] = {customer.PurchasesToDate}) WHERE [Cust_ID] = {id};", connection);
					command.ExecuteNonQuery();
					Customers[id] = customer;
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void SaveProduct(Product product)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"INSERT INTO {ProductTable} (Name, Price, Description, Publisher, Category) VALUES ('{product.Name}', {product.Price}, '{product.Description}', '{product.Publisher}', '{product.Category}');", connection);
					command.ExecuteNonQuery();
					Products.Add(Products.Keys.Last() + 1, product);
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void DeleteProduct(int id)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"DELETE FROM {ProductTable} WHERE [Prod_ID] = {id}", connection);
					command.ExecuteNonQuery();
					Products.Remove(id);
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void UpdateProduct(int id, Product product)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"UPDATE {ProductTable} SET [Name] = '{product.Name}', [Price] = {product.Price}, [Description] = '{product.Description}', [Publisher] = '{product.Publisher}', [Category] = '{product.Category}' WHERE [Prod_ID] = {id};", connection);
					command.ExecuteNonQuery();
					Products[id] = product;
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void SavePurchase(Purchase purchase)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"INSERT INTO {PurchaseTable} (Cust_ID, Prod_ID, Purchase_Date, Purchase_Price) VALUES ({Customers.Where(cust => cust.Value == purchase.Customer).Select(cust => cust.Key)}, {Products.Where(prod => prod.Value == purchase.Product).Select(prod => prod.Key)}, '{purchase.PurchaseDate}', {purchase.Price});", connection);
					command.ExecuteNonQuery();
					Purchases.Add(Purchases.Keys.Last() + 1, purchase);
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void DeletePurchase(int id)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"DELETE FROM {PurchaseTable} WHERE [Purchase_ID] = {id};", connection);
					command.ExecuteNonQuery();
					Purchases.Remove(id);
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}

		public void UpdatePurchase(int id, Purchase purchase)
		{
			try
			{
				string connectionString = ConfigurationManager.ConnectionStrings["cis341Connection"].ConnectionString;
				using (SqlConnection connection = new SqlConnection(connectionString))
				{
					connection.Open();
					SqlCommand command = new SqlCommand($"UPDATE {PurchaseTable} SET [Cust_ID] = {Customers.Where(cust => cust.Value == purchase.Customer).Select(cust => cust.Key)}, [Prod_ID] = {Products.Where(prod => prod.Value == purchase.Product).Select(prod => prod.Key)}, [Purchase_Date] = {purchase.PurchaseDate}, [Purchase_Price] = {purchase.Price} WHERE [Purchase_ID] = {id}", connection);
					command.ExecuteNonQuery();
					Purchases[id] = purchase;
				}
			}
			catch (SqlException e)
			{
				Console.WriteLine(e);
				throw;
			}
		}
	}
}