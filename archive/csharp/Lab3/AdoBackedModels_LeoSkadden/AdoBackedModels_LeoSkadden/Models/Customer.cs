﻿using System;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Customer
	{
		public Customer()
		{
		}

		public Customer(string firstName, string lastName, string address, string city, string state, string zip,
			string phone, string login, string password, DateTime birthDate, string comments, decimal purchases)
		{
			FirstName = firstName;
			LastName = lastName;
			Address = address;
			City = city;
			State = state;
			Zip = zip;
			Phone = phone;
			Login = login;
			Password = password;
			BirthDate = birthDate;
			Comments = comments;
			PurchasesToDate = purchases;
		}

		public string FirstName { get; set; }
		public string LastName { get; set; }
		public string Address { get; set; }
		public string City { get; set; }
		public string State { get; set; }
		public string Zip { get; set; }
		public string Phone { get; set; }
		public string Login { get; set; }
		public string Password { get; set; }
		public DateTime BirthDate { get; set; }
		public string Comments { get; set; }
		public decimal PurchasesToDate { get; set; }
	}
}