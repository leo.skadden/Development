﻿namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Product
	{
		public Product()
		{
		}

		public Product(string name, decimal price, string description, string publisher, string category)
		{
			Name = name;
			Price = price;
			Description = description;
			Publisher = publisher;
			Category = category;
		}

		public string Name { get; set; }
		public decimal Price { get; set; }

		public string Description { get; set; }

		//public byte[] Picture { get; set; }
		public string Publisher { get; set; }
		public string Category { get; set; }
	}
}