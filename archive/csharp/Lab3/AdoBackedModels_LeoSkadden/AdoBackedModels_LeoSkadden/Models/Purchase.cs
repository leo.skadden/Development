﻿using System;

namespace UsingViewsAndModels_LeoSkadden.Models
{
	public class Purchase
	{
		public readonly string CustomerFirstName;
		public readonly string CustomerLastName;
		public readonly string ProductName;

		public Purchase()
		{
		}

		public Purchase(Customer customer, Product product, DateTime purchaseDate, decimal price)
		{
			PurchaseDate = purchaseDate;
			Customer = customer;
			Product = product;
			Price = price;
			CustomerFirstName = Customer.FirstName;
			CustomerLastName = Customer.LastName;
			ProductName = Product.Name;
		}

		internal Customer Customer { get; }
		internal Product Product { get; }
		public DateTime PurchaseDate { get; set; }
		public decimal Price { get; set; }
	}
}