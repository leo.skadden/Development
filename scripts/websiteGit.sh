#!/bin/bash
set -ex

#Check flags
while getopts ":p:s:" opt; do
  case $opt in
    #Gitpath Flag
    p)
      gitpath=$opt
      ;;
    #Server Flag
    s)
      server=$opt
      ;;
    #Invalid Flag
    \?)
      echo "Invalid Option: -$OPTARG"
      exit 1
      ;;
  esac
done

#Check variable to see if they were set by the user, if not set a default value
if [ -z "$gitpath" ]
then
  gitpath="$HOME/git"
fi
if [ -z "$server" ]
then
  server="ssdnodes"
fi

mkdir -p $gitpath/website
