#!/bin/bash
set -xe

repoName=$1
serverRepoName="/srv/git/$repoName".git
server="website"

ssh $server mkdir -p "$serverRepoName"
ssh $server git init --bare "$serverRepoName"
ssh $server sudo chown -R git:git "$serverRepoName"
ssh $server sudo chmod -R 770 "$serverRepoName"

mkdir -p "$HOME/Development/$repoName"
cd "$HOME/Development/$repoName"
git init
echo Created by newGit script | cat > README
git add .
git commit -am 'Init by newGit.sh'
git remote add repo ssh://"$server":"$serverRepoName"
git push --set-upstream repo master