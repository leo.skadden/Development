cd /

tar --listed-incremental /mnt/storage/backup.snar  -cvpzf /mnt/storage/backup_full.tar.gz \
--exclude=/mnt/storage/backup.tar.gz \
--exclude=/proc \
--exclude=/tmp \
--exclude=/mnt \
--exclude=/dev \
--exclude=/sys \
--exclude=/run \
--exclude=/media \
--exclude=/var/log \
--exclude=/usr/src/linux-headers* \
--exclude=/home/*/.gvfs \
--exclude=/home/*/.cache \
--exclude=/home/*/.local/share/Trash \
--exclude=/home/*/.local/share/Steam \
--exclude=/home/*/Downloads /