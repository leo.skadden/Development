(ns euler.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (println "Hello, World!"))

(defn multiples-3-and-5
  "Find the sum of all multiples of 3 and 5 below 1000"
  []
  (reduce +
          (set (into (multiples-below-1000 3)
                     (multiples-below-1000 5)))))
           

(defn multiples-below-1000
  "Takes a number to find multiples of and returns a vecotr of multiples"
  [base]
  (loop [factor 1
         multiples [0]]
    (if (>= (last multiples) 1000)
      (drop-last multiples)
      (recur (+ factor 1)
             (into multiples
                   [(* base factor)])))))
