(ns euler.largest-palindrome-product
  (:gen-class))

(defn -main
  "Main"
  [num-of-digits & args]
  ())

(defn palindrome?
  "Takes a string and checks if it is the same forwards and backwards"
  [input]
  (let [lower-cased-input (clojure.string/lower-case input)]
    (= lower-cased-input
       (apply str (reverse lower-cased-input)))))

(defn make-starting-number
  [num-of-digits]
  (parse-int (apply str (repeat num-of-digits 9))))

(defn parse-int
  "Takes a string as input and returns the first continus number inside"
  [number]
  (Integer. (re-find #"\d+" number)))
