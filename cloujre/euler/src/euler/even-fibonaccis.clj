(ns euler.core
  (:gen-class))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (reduce + (filter even? (fibonaccis))))

(defn fibonaccis
  ""
  []
  (loop [previous 2
         two-ago 1
         fibs [1 2]]
    (if (>= previous 4000000)
      (drop-last fibs)
      (let [next (+ two-ago previous)]
           (recur next
                  previous
                  (into fibs [next]))))))
