(ns euler.largest-prime-factor
  (:gen-class))

(defn -main
  ""
  [n & args]
  (last (primeFactors n)))

(defn factors
  "Takes a number and gives a sorted set of matched factors."
  [n]
  (into (sorted-set)
        (mapcat (fn [x] [x (/ n x)])
                (filter #(zero? (rem n %)) (range 1 (inc (Math/sqrt n)))))))

(defn prime?
  "Returns true if the number is a prime number."
  [n]
  (= 2 (count (factors n))))

(defn primeFactors
  "Returns all prime factors of a number."
  [n]
  (into (sorted-set)
        (filter prime? (factors n))))
