(ns exercises.chapter-3
  (:gen-class))

(defn -main
  "I'm not sure if I need a main."
  [& args]
  (println "Unsureness"))

(defn one
  "Use the str, vector, list, hash-map, and hash-set functions"
  []
  (println (str "I still think " "interpolation is more readable, but I understand the limitations imposed " "by functionality"))
  (println (vector 1 2 3 "hi"))
  (println (list "hi" 4 3 2 1/2))
  (println  (hash-map :name "Leo" :grumpiness 7))
  (hash-set :name "Leo" :name "Leo" :grumpiness 7))

(defn two
  "Write a function that takes a number and adds 100 to it."
  [number]
  (+ number 100))

(defn dec-maker
  "3. Write a function, dec-maker, that works exactly like the function inc-maker except with subtraction"
  [decrement-amount]
  (fn
    [number]
    (- number decrement-amount)))

(defn mapset
  "4. Write a function, mapset, that works like map except the return value is a set"
  [function values]
  (set (map function values)))

(defn expand-body-parts-by-five
  "Expects a seq of maps that have a :name and :size"
  [partial-body-parts]
  (defn expand-part
    [part]
    (loop [replacements ["left-middle-" "middle-" "right-middle-" "right-"]
           body-parts []]
      (if (empty? replacements)
        body-parts
        (let [[head & tail] replacements]
          (recur tail
                 (into body-parts
                       [{:name (clojure.string/replace (:name part) #"^left-" head)
                          :size (:size part)}] ))))))

  (reduce (fn [final-body-parts part]
            (into final-body-parts (set (into [part] (expand-part part)))))
          []
          partial-body-parts))

(defn expand-body-parts
  "Expects a seq of body parts and a number of times to expand"
  [partial-body-parts total-parts]
  )
