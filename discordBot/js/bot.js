const Discord = require('discord.js');
const client = new Discord.Client();
const auth = require('./auth.json');
const prefix = '!anonymous-bot';
const config = require('./config.json');
const fs = require('fs');
let count = 0;
let channel;

// Make some noise when it starts up.
client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
    channel = config.channel;
});

// Monitor for messages so it can, ya know, do stuff.
client.on('message', message => {
    // If the bot says something ignore it.
    if (message.author.tag === client.user.tag) {
        return;
    }

    // If the bot is being talked to in a DM we will send the message in
    // the channel that is set.
    if (message.channel.type === 'dm') {
        message.reply('I will tell the world your secret.');
        message.reply(message.attachments.array().length);
        channel.send(`${message.content}`);
        if (message.attachments.array().length > 0) {
            for (let attachment of message.attachments.array()) {
                channel.send(attachment);
            }
        }
        return;
    }

    // If the channel isn't a DM and the author of the message is an admin
    // make sure they aren't trying to give us a command.
    if (message.channel.permissionsFor(message.author).has('ADMINISTRATOR')) {
        adminCommands(message);
        return;
    }
});

// Command logic
let adminCommands = (message) => {
    // Bail out if the message didn't start with the prefix
    if (! message.content.startsWith(prefix)) {
        return;
    }

    // Remove the prefix and set the arguments to be every word and 
    // filter any empty strings. Then set the main command to the first word
    // and remove it from the argument array.
    const args = message.content.slice(prefix.length).split(/ +/).filter(x => x !== "");
    const command = args.shift().toLowerCase();

    // Command to set the secrets channel.
    if (command === 'set-channel') {
        // Just in case it's set in a voice channel this will make sure we
        // don't throw any undefined errors.
        if (message.channel.type !== 'text') {
            message.reply('Please only use a text channel.');
            return;
        }

        // Set the channel object to the channel the message was sent from,
        // then set the config object to hold the new channel, then write
        // the new config to disk.
        channel = message.channel;
        config.channel = channel;
        fs.writeFile("./config.json",
                     JSON.stringify(config),
                     "utf8", 
                     function(err){fileWriteCallback(err, message);});
        message.reply(`Setting the anonymous channel to ${channel.name}`);
    }
};

// How to handle an error while writing to disk.
let fileWriteCallback = (error, message) => {
    if (error) {
        message.reply(error);
        return;
    }
};

client.login(auth.token);
