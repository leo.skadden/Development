def sample():
    import random
    probabilityOfRain = 0.4
    trueCount = 0
    for x in range(0,100):
        sample = random.random()
        if sample < probabilityOfRain:
            print(str(x)+": true")
            trueCount = trueCount + 1
    print("Total true cases: " + str(trueCount))

if __name__ == "__main__":
    sample()