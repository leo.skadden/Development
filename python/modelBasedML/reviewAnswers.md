# Model Based Machine Learning
## Chapter 1
### Section 1
1. Estimate probabilities
  1. Question: After visiting a product page on amazon the user buys the product.
   Answer: I would estimate that very few purchase are made based on sole product page visits. The product page is where a user can find information and reviews about the project so there are probably very low click throughs. P(.2)
  2. Question: A user gets a email and they choose to reply.
   Answer: Based on my own email usage, personal and professional, I have to say this is very very low. P(.01)
  3. Question: It will rain tomorrow.
   Answer: It's fall and it's been rainy the last couple days but today is sunnny so P(.4)
  4. Question: A murderer is related to their victim.
   Answer: If I recall correctly the probability of this is very high. P(.8)
2. Rewrite the above probabilities as bernulli distubutions, short and long.
  1. Bernoulli(purchase;.2) Bernoulli(.2)
  2. Bernoulli(reply;.01) Bernoulli(.01)
  3. Bernoulli(rain;.4) Bernoulli(.4)
  4. Bernoullie(related;.8) Bernoulli(.8)
3. Question: I am certain that it will rain tomorrow what is the bernoulli distrubution of this and the inverse? What if I'm completely unsure?
 Answer: Bernoulli(rain;1) is 100% chance of rain tomorrow Bernoulli(rain;0) is 0% chance of rain. Bernoulli(rain;.5) is totaly unsure.
4. Write a program. RandomSample.py

### Section 2
1. Estimate conditional properties
  1. Question: Being late for work if traffic is bad
   Answer: I live in a small town without any real traffic so I leave very close to the time I'm supposed to be at work, lots of traffic would not be good for my time table. P(late|traffic) = 0.9
  2. Question: A user replies to an email if they know the sender
   Answer: P(reply|knownSender) = 0.6
  3. Question: It will rain on day X if it rained on day X - 1
   Answer: P(rain|rainYesterday) = 0.6
2. Choose a situation in your life that is represented by two binary variable one conditional on the other.
   enjoyClass is a true or false variable that describes weither or not I like a given class. goodTeacher is a true or false variable that describes if I think the professor is good at teaching.

|GoodTeacher|enjoyClass = true|enjoyClass = false|
|___________|_________________|__________________|
| True      | 0.8             | 0.2              |
| False     | 0.1             | 0.9              |

