import re
import collections


def find_words(text): return re.findall('[a-z]+', text.lower())


def train(features):
    model = collections.defaultdict(lambda: 1)
    for feature in features:
        model[feature] += 1
    return model


wordsInFile = find_words(open('big.txt').read())
knownWords = train(wordsInFile)
alphabet = 'abcdefghijklmnopqrstuvwxyz'


def edits(word):
    splits = [(word[:i], word[i:]) for i in range(len(word) + 1)]
    deletes = [a + b[1:] for a, b in splits if b]
    transposes = [a + b[1] + b[0] + b[2:] for a, b in splits if len(b) > 1]
    replaces = [a + c + b[1:] for a, b in splits for c in alphabet if b]
    inserts = [a + c + b for a, b in splits for c in alphabet]
    return set(deletes + transposes + replaces + inserts)


def known_edits(word):
    return set(knownEdit for edit in edits(word) for knownEdit in edits(edit) if knownEdit in knownWords)


def known(words): return set(word for word in words if word in knownWords)


def correct(word):
    candidates = known([word]) or known(edits(word)) or known_edits(word) or [word]
    return max(candidates, key=knownWords.get)
