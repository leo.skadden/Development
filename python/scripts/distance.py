def main():
  nInput = raw_input("What is n?\n")
  try: 
    n = int(nInput)
  except ValueError:
    print("Not a number.")
    return

  xInput = raw_input("Please enter a space delimited array of numbers.\n")
  x = list(map(int, xInput.split(" ")))

  yInput = raw_input("Please enter a second space delimited array of numbers.\n")
  y = list(map(int, yInput.split(" ")))

  print(y)

  print (normDistance(n, x, y))

def normDistance(n, x, y):
  validInput = validateInputs(n, x, y)
  if validInput[0] == False:
    print(validInput[1])
    return
  
  exponent = float(1 / n)
  return normDistanceSum(n, x, y) ** exponent

  
def normDistanceSum(n, x, y):
  distanceFormula = lambda a, b: float(abs(a - b) ** n)
  print (x[0] y[0])
  print distanceFormula(x[0], y[0])

  if len(x) == 1:
    # Base case
    return distanceFormula(x[0], y[0])

  # Recursion
  return distanceFormula(x[0], y[0]) + normDistanceSum(n, x[1:], y[1:])

def validateInputs(n, x, y):
  # Check for x and y length equal
  if len(x) != len(y):
    return (False, "X and Y must have equal dimensions.")

  # Check that n is greater than 1
  if n < 1:
    return (False, "n must be at least 1.") 
  return (True, "")

if __name__ == "__main__":
  main()