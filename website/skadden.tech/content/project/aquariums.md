+++
title = "Aquariums"
date = 2019-01-16T19:21:30-06:00
draft = false

# Tags: can be used for filtering projects.
# Example: `tags = ["machine-learning", "deep-learning"]`
tags = []

# Project summary to display on homepage.
summary = ""

# Optional image to display on homepage.
image_preview = ""

# Optional external URL for project (replaces project detail page).
external_link = ""

# Does the project detail page use math formatting?
math = false

# Does the project detail page use source code highlighting?
highlight = false

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
[header]
image = ""
caption = ""

+++

One of my favorite hobbies is keeping aquariums. Currently I have three aquariums in my apartment: one 5 gallon [Marineland Portrait](http://www.marineland.com/products/aquariums/portrait-desktop-kit.aspx) aquarium for a beautiful Betta (This one is technically my fiance's but I take care of it), a ten gallon community tank that is waiting to be upgraded to a larger tank, and a 20 gallon long that has two [Axolots](https://en.wikipedia.org/wiki/Axolotl).

## How I Started
