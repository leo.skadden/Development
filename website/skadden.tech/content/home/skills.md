+++
# Feature/Skill widget.
widget = "featurette"  # Do not modify this line!
date = 2017-09-20T00:00:00

# Activate this widget? true/false
active = true

title = "Skills"
subtitle = ""

# Order that this section will appear in.
weight = 7

# Showcase personal skills or business features.
# 
# Add/remove as many `[[feature]]` blocks below as you like.
# 
# Available icon packs and icons:
# * fas - Font Awesome standard icons (see https://fontawesome.com/icons)
# * fab - Font Awesome brand icons (see https://fontawesome.com/icons)
# * ai - academic icons (see https://jpswalsh.github.io/academicons/)

[[feature]]
  icon = "microsoft"
  icon_pack = "fab"
  name = ".Net"
  description = ""
  
[[feature]]
  icon = "java"
  icon_pack = "fab"
  name = "Java"
  description = ""  
  
 [[feature]]
  icon = "linux"
  icon_pack = "fab"
  name = "Linux"
  description = ""

[[feature]]
  icon = "html5"
  icon_pack = "fab"
  name = "HTML 5"
  description = ""

[[feature]]
  icon = "css3"
  icon_pack = "fab"
  name = "CSS 3"
  description = ""

[[feature]]
  icon = "js"
  icon_pack = "fab"
  name = "JavaScript"
  description = ""
+++
