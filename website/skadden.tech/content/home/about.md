+++
# About/Biography widget.
widget = "about"
active = true
date = 2016-04-20T00:00:00

# Order that this section will appear in.
weight = 5

# List your academic interests.
[interests]
  interests = [
    "Backend Development",
    "Linux"
  ]

# List your qualifications (such as academic degrees).
[[education.courses]]
  course = "B.S. Computer Information Systems"
  institution = "University of Wisconsin - Stevens Point"
  year = 2019

+++

# Biography

I am a Software Developer in my last year of a B.S. in Computer Information Systems at UWSP. I've been working at the Sentry Insurance IT Co-op program since June 2017. I've worked on Java and .NET projects there including a policy rating API and a data processing, MVC based, web app to replace a mainframe system. I've also had the opportunity to work with angular, C programming on arduino, and Hololens development. In my personal time I enjoy cooking dinner for my fiance and myself, spending time with my 2 cats or 2 rabbits (Did I mention my fiance is pre-vet?), or watching my tropical community aquarium or my Axolotl aquarium.
