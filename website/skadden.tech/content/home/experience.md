+++
# Experience widget.
widget = "experience"  # Do not modify this line!
active = true  # Activate this widget? true/false

title = "Experience"
subtitle = ""

# Order that this section will appear in.
weight = 8

# Date format for experience
#   Refer to https://sourcethemes.com/academic/docs/customization/#date-format
date_format = "January 2006"

# Experiences.
#   Add/remove as many `[[experience]]` blocks below as you like.
#   Required fields are `title`, `company`, and `date_start`.
#   Leave `date_end` empty if it's your current employer.
#   Begin/end multi-line descriptions with 3 quotes `"""`.
[[experience]]
  title = "Associate Software Developer"
  company = "Sentry Insurance"
  company_url = ""
  location = "Stevens Point, WI"
  date_start = "2017-07-01"
  date_end = ""
  description = """
  Responsibilities include:
  
  * Developing in AGILE environment with limited oversight
  * Teaching and learning from peers
  * Satisfying end user requirements
  """

[[experience]]
  title = "Cabling and Infrastructure Technician"
  company = "UW Stevens Point"
  company_url = ""
  location = "Stevens Point, WI"
  date_start = "2016-03-01"
  date_end = "2017-05-31"
  description = """
  Solved user issues for campus telephone and internet systems including:

  * POTS system
  * IP phones
  * Ethernet
  * Wifi
  """

+++
