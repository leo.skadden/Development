+++
title = "Hello!"
date = 2018-10-13T20:02:09-05:00
draft = false

# Authors. Comma separated list, e.g. `["Bob Smith", "David Jones"]`.
authors = ["Leo Skadden"]

# Tags and categories
# For example, use `tags = []` for no tags, or the form `tags = ["A Tag", "Another Tag"]` for one or more tags.
tags = ["This website", "Leo Skadden"]
categories = []

# Featured image
# Place your image in the `static/img/` folder and reference its filename below, e.g. `image = "example.jpg"`.
# Use `caption` to display an image caption.
#   Markdown linking is allowed, e.g. `caption = "[Image credit](http://example.org)"`.
# Set `preview` to `false` to disable the thumbnail in listings.
[header]
image = ""
caption = ""
preview = true

+++
I've finally done it. I've had a personal website of some sort or another in my repository since I took my intro to HTML class in my sophomore year. The first draft I had was abandoned because I didn't have the design skills that I thought I needed to make it look how I wanted. 

I still don't have the design skills to make something that I really like today, skills like choosing a good looking color palette and keeping to it or tying elements of the page into others in order to develop a theme for the website. These have always been very difficult for me to make any progress on. I don't know if I'm falling pray to the creative process,if I pushed through it would end up something to be proud of?

The next draft was a website that I put together using a CSS theme that I had found. This helped eliminated the need for me to create a design aesthetic on my own but I was still left with a website that didn't feel cohesive. I was also worried that I would be judged by peers and potential employers if my website was clearly using a theme or template. I thought that if I didn't do everything from scratch I would be viewed as a hack or a lazy developer.

Finally I decided that I wanted to have a website by the end of Fall semester my senior year. The motivation for this was in part to have a place to write about whatever project I'm currently dabbling in, in part to make better use of my VPS, and to have a modern resume/web presence for my job search.

We'll see if I keep up with blog style posts. If not this may get redesigned to be a place to hold an online version of my resume and put one off pages about projects.