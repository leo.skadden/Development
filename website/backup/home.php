<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Leo Skadden</title>
		<link rel="stylesheet" href="/styles/tufte.css"/>
	</head>
	<body>
		<?php
			require_once('nav.html');
			print(file_get_contents(nav.html));
		?>
		<main>
			<h1>Leo Skadden</h1>
				<h2>Who am I?</h2>
					<ul>
						<li>Student at UW Stevens Point:
							<ul>
								<li id="schoolMajor">Major: Computer Information Systems</li>
								<li id="schoolMinor">Minor: Human Technology Interaction</li>
							</ul>
						</li>
						<li>Treasurer of the Linux User Group</li>
						<li>Associate at the Sentry Insurance Stevens Point Co-op</li>
						<li>Aspiring maker</li>
					</ul>
				<h2>What's going on here?</h2>
					<p>
						I'm making this site to give me a place to ramble about projects and other stuff.
					</p>
					<h3>Todo:</h3>
					<ul>
						<li><s>Setup automatic deployment</s> Done! Take a look <a href="http://ryanflorence.com/deploying-websites-with-a-tiny-git-hook/">here</a> for the reference I used.</li>
						<li>Setup elk</li> <!--https://www.digitalocean.com/community/tutorials/how-to-install-elasticsearch-logstash-and-kibana-elk-stack-on-ubuntu-16-04-->
						<li>Setup subdomains, <s>blog.skadden.tech</s> repo.skadden.tech, etc</li>
						<li>Stylesheets</li>
						<li>Resume page</li>
						<li>Contact info</li>
						<li><s>Setup leo@skadden.tech</s> Done!</li>
						<li>Personal Projects Page</li>
						<li>Setup blog</li>
					</ul>
		</main>
	</body>
</html>
