package tech.skadden.games.crazyeights.cardcollection;

import org.junit.jupiter.api.Test;
import tech.skadden.games.crazyeights.card.WildCard;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.fail;

class DeckCompositionTests {
	@Test
    void test52TotalCards() {
		final var deck = new Deck();
		assertThat(deck.getCount())
			.as("A deck should have 52 cards.")
			.isEqualTo(52);
	}

	@SuppressWarnings("DuplicatedCode")
    @Test
    void testFourOfEachRank() {
		final var deck = new Deck();

		var twos = 0;
		var threes = 0;
		var fours = 0;
		var fives = 0;
		var sixes = 0;
		var sevens = 0;
		var eights = 0;
		var nines = 0;
		var tens = 0;
		var jacks = 0;
		var queens = 0;
		var kings = 0;
		var aces = 0;

		for (final var card : deck.cards) {
			switch (card.getRank()) {
				case None:
					break;
				case Two:
					twos++;
					break;
				case Three:
					threes++;
					break;
				case Four:
					fours++;
					break;
				case Five:
					fives++;
					break;
				case Six:
					sixes++;
					break;
				case Seven:
					sevens++;
					break;
				case Eight:
					eights++;
					break;
				case Nine:
					nines++;
					break;
				case Ten:
					tens++;
					break;
				case Jack:
					jacks++;
					break;
				case Queen:
					queens++;
					break;
				case King:
					kings++;
					break;
				case Ace:
					aces++;
					break;
			}
		}

		assertThat(twos)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(threes)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(fours)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(fives)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(sixes)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(sevens)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(eights)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(nines)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(tens)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(jacks)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(queens)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(kings)
			.as("There should be four of each rank")
			.isEqualTo(4);
		assertThat(aces)
			.as("There should be four of each rank")
			.isEqualTo(4);
	}

	@Test
    void testThirteenOfEachSuit() {
		final var deck = new Deck();

		int clubs = 0;
		int spades = 0;
		int hearts = 0;
		int diamonds = 0;

		for (final var card : deck.cards) {
			switch (card.getSuit()) {
				case Clubs:
					clubs++;
					break;
				case Spades:
					spades++;
					break;
				case Hearts:
					hearts++;
					break;
				case Diamonds:
					diamonds++;
					break;
				default:
                    //noinspection ResultOfMethodCallIgnored
                    fail("There should have only been four suits in the deck.");
			}
		}

		assertThat(clubs)
			.as("There should be 13 clubs.")
			.isEqualTo(13);
		assertThat(spades)
			.as("There should be 13 spades.")
			.isEqualTo(13);
		assertThat(hearts)
			.as("There should be 13 hearts.")
			.isEqualTo(13);
		assertThat(diamonds)
			.as("There should be 13 diamonds.")
			.isEqualTo(13);
	}

	@Test
	void testFourWildCards() {
		final var deck = new Deck();

		int wildcards = 0;

		for (final var card : deck.cards) {
			if (card instanceof WildCard) {
				wildcards++;
			}
		}

		assertThat(wildcards)
			.as("There should be four wildcards in a crazy eights deck.")
			.isEqualTo(4);
	}
}