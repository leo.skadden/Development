package com.sentry.games.structured;

public enum Suit {
    None (0),
    Hearts (1),
    Diamonds (2),
    Clubs (3),
    Spades (4);
	
	private int suit;
	Suit(){}
	
	Suit(final int suit){
		this.suit = suit;
	}
		
	public int getSuit() {
		return this.suit;
	}
}
