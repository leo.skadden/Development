package com.sentry.games.structured;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

class BaseGame {

    public static void main(final String[] args) {
        final List<Card> deck = new ArrayList<>();
        final List<Card> discardPile = new ArrayList<>();
        final List<Card> stockPile = new ArrayList<>();
        final List<String> players = new ArrayList<>();
        final List<ArrayList> hands = new ArrayList<>();
        final int currentPlayerIndex = 0;
        final Suit masqueradeSuit = Suit.None;
        final String winner = "";


    }

    private void loadDeckWithCards(final ArrayList<Card> deck) {
        final Suit[] suits = Suit.values();
        final Rank[] ranks = Rank.values();
        for (final Suit suit : suits) {
            for (int z = 0; z < suits.length; z++) {
                final Rank rank = ranks[z];
                final Card card = new Card();
                card.rank = rank;
                card.suit = suit;
                deck.add(card);
            }
        }
    }

    private void shuffleCards(final ArrayList<Card> cards) {
        Collections.shuffle(cards);
    }

    private void setupPlayers(final ArrayList<String> players) {
        //TODO setupPlayers
//    	Console.WriteLine("Welcome to Crazy Eights!")
//	    Console.Write("How many players are there (2-6)? ==> ")

//	    int numberOfPlayers = CInt(Console.ReadLine)
        final int numberOfPlayers = 5;

        for (int playerNumber = 1; playerNumber <= numberOfPlayers; playerNumber++) {
            //TODO add player name from console
            //Console.Write("Enter the name of player " & playerNumber.ToString & " ==> ")
            //Dim playerName = Console.ReadLine
            final String playerName = "Player Name " + playerNumber;
            players.add(playerName);
        }
    }


    private void dealCardsToHands(final ArrayList<Card> deck, final ArrayList hands) {

        for (final Object o : hands) {
            ArrayList<Card> hand = (ArrayList<Card>) o;
            if (hand == null || hand.isEmpty()) {
                hand = new ArrayList<>();
            }
            for (int cardIndex = 1; cardIndex <= 5; cardIndex++) {
                final Card topCard = deck.get(0);
                deck.remove(topCard);
                hand.add(topCard);
            }
        }
    }

    private void setupPiles(final ArrayList<Card> deck, final ArrayList<Card> discardPile, final ArrayList<Card> stockPile) {
        //TODO add real code.  Not sure what add range does
//    	stockPile.addRange(deck)
//    	deck.Clear()
//    	Dim topCard = stockPile(0)
//    	stockPile.Remove(topCard)
//    	discardPile.Add(topCard)
    }
}
