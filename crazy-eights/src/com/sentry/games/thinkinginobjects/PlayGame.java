package com.sentry.games.thinkinginobjects;

import tech.skadden.games.crazyeights.card.Card;
import tech.skadden.games.crazyeights.card.Suit;
import tech.skadden.games.crazyeights.card.WildCard;

import java.util.Scanner;

class PlayGame {
	private static Game game;
	
	public static void main(final String[] args) {
		configureGame();
        do {
       	        	
	        displayDiscardPileTopCard();
	        displayCurrentPlayer();
	        displayCards();
	
	        final Card cardToPlay = getCardToPlay();
	
	        if(cardToPlay == null){
	            final Card drawnCard = game.drawCard();
	            displayDrawnCard(drawnCard);
	        }else{
	        	if(cardToPlay instanceof WildCard){
	                final Suit suitToPlay = getMasqueradeSuit();
	                //((WildCard)cardToPlay).setMasqueradeSuit(suitToPlay);
	        	}
	
	            try{
	                game.playCard(cardToPlay);
	            }
	            catch (final Exception ex){
	            	System.out.println(ex.getMessage());
	            }
	
	        }
	
		}while(!game.hasWinner());
        displayWinner();
	}

	
    private static void configureGame(){

    	game = new Game();

    	
    	System.out.println("Welcome to Crazy Eights!");
    	System.out.println("How many players are there (2-6)? ==> ");
    	final Scanner in = new Scanner(System.in);
    	final int numberOfPlayers = in.nextInt();
    	//in.close();

    	for (int playerNumber = 1; playerNumber <= numberOfPlayers; playerNumber++){
    		
    		System.out.println("Enter the name of player " + playerNumber + " ==> ");
        	final String playerName = new Scanner(System.in).nextLine();
        	final Player player = new Player(playerName);
        	game.addPlayer(player);
    	}

    	game.beginGame();
	}
    
    private static void displayDiscardPileTopCard(){
    	System.out.println("The discard pile has a top card of " + game.topDiscardPileCard().toString());
    }

    private static void displayCurrentPlayer(){
    	System.out.println();
    	System.out.println("Current player: " + game.currentPlayer().getName());
    }

    private static void displayCards(){
    	System.out.println();
    	System.out.println("The following cards are in your hand:");
    	System.out.println();
    	final int cardIndex = 1;
//    	for (Card card : game.currentPlayer().getHand().getCards()){
//    		System.out.println(cardIndex +": "+ card.toString());
//    		cardIndex++;
//    	}
	}

	private static Card getCardToPlay(){
		System.out.println();
		System.out.println("Which card do you wish to play? (Hit enter to draw) ==> ");
		final String cardIndex = new Scanner(System.in).nextLine();

	    if (cardIndex.trim().equals("")){
	    	return null;
	    }
	    else{
	        return null; // game.currentPlayer().getHand().getCards().get(Integer.parseInt(cardIndex) - 1);
	    }
	}

	private static void displayDrawnCard(final Card card){
		System.out.println("You drew a " + card.toString());
	}

	private static Suit getMasqueradeSuit(){ 
	    System.out.println("Which suit do you want to play? (H)earts (D)iamonds (C)lubs or (S)pades ==> ");
	    final String suit = new Scanner(System.in).nextLine();
	    switch(suit.charAt(0)){
	        case 'H':
	            return Suit.Hearts;
	        case 'D':
	            return Suit.Diamonds;
	        case 'C':
	            return Suit.Clubs;
	        case 'S':
	            return Suit.Spades;
	        default:
	        	return null;
	    }
	}

	private static void displayWinner(){
		System.out.println(game.getWinner().getName() + " has won the game!");
	}    
    
}
