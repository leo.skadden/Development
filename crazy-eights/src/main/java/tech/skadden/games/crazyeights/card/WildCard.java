package tech.skadden.games.crazyeights.card;

public class WildCard extends Card {

	public WildCard(final Rank rank, final Suit suit) {
		super(rank, suit);
	}

	public WildCard(final Card card) {
		super(card);
	}

	public void setSuit(final Suit suit) {
		this.card.suit = suit;
	}

	@Override
	public boolean canBePlacedOn(final CardStructure card) {
		return true;
	}

	public String toString() {
		return "Wildcard acting as " + super.toString();
	}
}
