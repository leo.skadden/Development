package tech.skadden.games.crazyeights.game;

import tech.skadden.games.crazyeights.card.Card;
import tech.skadden.games.crazyeights.cardcollection.CardCollection;
import tech.skadden.games.crazyeights.cardcollection.Deck;

import java.util.ArrayList;
import java.util.List;

class Game {
	
    private final List <Player> players = new ArrayList<>();
    private final CardCollection stockPile;
    private final CardCollection discardPile;
    Player winner = null;
    private int currentPlayerIndex;
	
    public Game(){
	    this.stockPile = new Deck();
	    this.discardPile = new CardCollection();
	    this.stockPile.shuffle();
    }

	public void addPlayer(final Player player){
		this.players.add(player);
	}
    
    public void beginGame(){
    	for(int i = 1;i <= 5;i++){
    		for (final Player player : this.players){
    			//deck.transferTopCard(player.getHand());
    		}
    	}
    }
    
    public Player currentPlayer() {
    	return this.players.get(this.currentPlayerIndex);
    }
    
    public Card topDiscardPileCard(){
    	return this.discardPile.topCard();
    }
    
    public void playCard(final Card card) throws Exception{
	    if(true/*card.canBePlacedOn(discardPile.topCard())*/) {
	        //currentPlayer().getHand().transferCard(card, discardPile);
	        if(currentPlayer().isWinner()){
		        this.winner = currentPlayer();
	        }
	        moveToNextPlayer();
	    }else{
	        throw new Exception("Unable to play a " + card.toString() + " on a " + this.discardPile.topCard().toString());
	    }
    }

    public Card drawCard(){
    	final Card drawnCard = this.stockPile.topCard();
    	//stockPile.transferTopCard(currentPlayer().getHand());
    	if(this.stockPile.getCount() == 0){
    		turnDiscardPileIntoStockPile();
    	}
    	moveToNextPlayer();
    	return drawnCard;
	}    
    
    public boolean hasWinner(){
        return this.winner != null;
    }
    
    private void moveToNextPlayer(){
	    this.currentPlayerIndex += 1;
    	if(this.currentPlayerIndex >= this.players.size()){
		    this.currentPlayerIndex = 0;
    	}
    }
    
    private void turnDiscardPileIntoStockPile(){
    	final Card discardPileTopCard = this.discardPile.topCard();
    	do{ 
    		//discardPile.transferTopCard(stockPile);
    	}while(this.discardPile.getCount() > 0);
        //stockPile.transferCard(discardPileTopCard, discardPile);
	    this.stockPile.shuffle();
	}
    
}
