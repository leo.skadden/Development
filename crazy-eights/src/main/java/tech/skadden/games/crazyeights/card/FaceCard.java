package tech.skadden.games.crazyeights.card;

public class FaceCard extends Card {
	public FaceCard(final Rank rank, final Suit suit) {
		super(rank, suit);
	}

	@Override
	public boolean canBePlacedOn(final CardStructure card) {
		if (super.canBePlacedOn(card)) {
			return true;
		}

		if (isFaceCard(card.rank)) {
			return this.colorOfSuit(super.card.suit) == colorOfSuit(card.suit);
		}

		return false;
	}

	private boolean isFaceCard(final Rank rank) {
		switch (rank) {
			case Jack:
			case King:
			case Queen:
			case Ace:
				return true;
			default:
				return false;
		}
	}

	private SuitColor colorOfSuit(final Suit suit) {
		switch (suit) {
			case Clubs:
			case Spades:
				return SuitColor.Black;
			default:
				return SuitColor.Red;
		}
	}

	private enum SuitColor {Black, Red}
}
