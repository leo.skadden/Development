package tech.skadden.games.crazyeights.card;

class CardStructure {
	Suit suit;
	Rank rank;

	CardStructure(final Rank rank, final Suit suit) {
		this.rank = rank;
		this.suit = suit;
	}
}
