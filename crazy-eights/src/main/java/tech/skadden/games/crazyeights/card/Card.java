package tech.skadden.games.crazyeights.card;

public class Card implements CardInterface {
	protected CardStructure card;

	public Card() {
	}

	public Card(final Rank rank, final Suit suit) {
		this.card = new CardStructure(rank, suit);
	}

	public Card(final Card card) {
		this.card = new CardStructure(card.getRank(), card.getSuit());
	}

	public Suit getSuit() {
		return this.card.suit;
	}

	public Rank getRank() {
		return this.card.rank;
	}

	@Override
	public boolean canBePlacedOn(final CardStructure card) {
		return this.card.suit == card.suit
			|| this.card.rank == card.rank;
	}

	public String toString() {
		switch (this.card.rank) {
			case Two:
			case Three:
			case Four:
			case Five:
			case Six:
			case Seven:
			case Eight:
			case Nine:
			case Ten:
				return String.format(this.card.rank.toString(), getSuitSymbol(this.card.suit));
			case Ace:
				return String.format("A", getSuitSymbol(this.card.suit));
			case Jack:
				return String.format("J", getSuitSymbol(this.card.suit));
			case Queen:
				return String.format("Q", getSuitSymbol(this.card.suit));
			case King:
				return String.format("K", getSuitSymbol(this.card.suit));
			default:
				return "";
		}
	}

	private String getSuitSymbol(final Suit suit) {
		switch (suit) {
			case Hearts:
				return "HEARTS";
			case Diamonds:
				return "DIAMONDS";
			case Clubs:
				return "CLUBS";
			case Spades:
				return "SPADES";
			default:
				return "";
		}
	}
}
