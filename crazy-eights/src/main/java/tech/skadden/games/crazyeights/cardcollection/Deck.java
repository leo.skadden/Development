package tech.skadden.games.crazyeights.cardcollection;

import tech.skadden.games.crazyeights.card.Card;
import tech.skadden.games.crazyeights.card.Rank;
import tech.skadden.games.crazyeights.card.Suit;
import tech.skadden.games.crazyeights.card.WildCard;

import java.util.ArrayList;
import java.util.List;

public class Deck extends CardCollection {
	public Deck() {
		final var deck = new ArrayList<Card>();
		for (final Suit suit : Suit.valuesWithoutNone()) {
			for (final Rank rank : Rank.valuesWithoutNone()) {
				deck.add(new Card(rank, suit));
			}
		}

		this.cards = makeEightsWild(deck);
	}

	private static List<Card> makeEightsWild(final List<Card> deck) {
		final List<Card> deckWithWildcards = new ArrayList<>();
		for (final Card card : deck) {
			if (card.getRank() != Rank.Eight) {
				deckWithWildcards.add(new Card(card));
				continue;
			}

			deckWithWildcards.add(new WildCard(card));
		}
		return deckWithWildcards;
	}
}
