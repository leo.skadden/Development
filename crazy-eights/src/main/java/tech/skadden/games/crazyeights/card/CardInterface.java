package tech.skadden.games.crazyeights.card;

public interface CardInterface {
	CardStructure card = null;

	boolean canBePlacedOn(CardStructure card);

	String toString();
}
