package tech.skadden.games.crazyeights.game;

import tech.skadden.games.crazyeights.cardcollection.CardCollection;

public class Player {
	public String name;
	public CardCollection hand = new CardCollection();

	public Player(final String name){
		this.name = name;
	}

	public boolean isWinner(){
		return this.hand.getCount() == 0;
	}
	    	
}
