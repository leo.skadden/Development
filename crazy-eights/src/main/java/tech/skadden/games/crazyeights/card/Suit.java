package tech.skadden.games.crazyeights.card;

public enum Suit {
	None(0),
	Hearts(1),
	Diamonds(2),
	Clubs(3),
	Spades(4);

	public int suit;

	Suit() {
	}

	Suit(final int suit) {
		this.suit = suit;
	}

	public static final Suit[] valuesWithoutNone() {
		return new Suit[]{Hearts, Diamonds, Clubs, Spades};
	}
}
