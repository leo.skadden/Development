package tech.skadden.games.crazyeights.cardcollection;

import tech.skadden.games.crazyeights.card.Card;

public interface CardCollectionInterface {
	void shuffle();

	Card takeCard(Card card);

	Card takeTopCard();

	Card topCard();

	Card cardAt(int index);

	Integer getCount();
}
