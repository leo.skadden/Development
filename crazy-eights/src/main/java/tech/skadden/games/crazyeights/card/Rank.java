package tech.skadden.games.crazyeights.card;

public enum Rank {
	None(0),
	Two(2),
	Three(3),
	Four(4),
	Five(5),
	Six(6),
	Seven(7),
	Eight(8),
	Nine(9),
	Ten(10),
	Jack(11),
	Queen(12),
	King(13),
	Ace(14);

	public int rank;

	Rank() {
	}

	Rank(final int rank) {
		this.rank = rank;
	}

	public static final Rank[] valuesWithoutNone() {
		return new Rank[]{Two, Three, Four, Five, Six, Seven, Eight, Nine, Ten, Jack, Queen, King, Ace};
	}
}
