package tech.skadden.games.crazyeights.cardcollection;

import tech.skadden.games.crazyeights.card.Card;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CardCollection implements CardCollectionInterface {
	List<Card> cards = new ArrayList<>();

	@Override
	public void shuffle() {
		Collections.shuffle(this.cards);
	}

	@Override
	public Card takeCard(final Card card) {
		this.cards.remove(card);
		return card;
	}

	@Override
	public Card takeTopCard() {
		return takeCard(topCard());
	}

	@Override
	public Integer getCount() {
		return this.cards.size();
	}

	@Override
	public Card topCard() {
		return this.cards.get(getCount() - 1);
	}

	@Override
	public Card cardAt(final int index) {
		return this.cards.get(index);
	}
}
