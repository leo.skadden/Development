#This is the simplified zshrc without comments that I don't want
export ZSH=/home/alkenes/.oh-my-zsh

##################### Oh-My-Zsh settings
ZSH_THEME="agnoster"
HYPHEN_INSENSITIVE="true"
ENABLE_CORRECTION="true"
COMPLETION_WAITING_DOTS="true"
plugins=(
  git
)
source $ZSH/oh-my-zsh.sh

##################### Aliases
# ssh-ident
alias scp='BINARY_SSH=scp ~/Git/ssh-ident/ssh-ident'
alias sftp='BINARY_SSH=sftp ~/Git/ssh-ident/ssh-ident'
# vi
alias vi=neovim
# eopkg
alias eokpg=eopkg
alias it="sudo eopkg it"
alias sr="eopkg sr"

##################### Functions
mc () {
	mkdir "$1"
	cd "$1"
}
